<a name=""></a>
##  (2021-02-20)


#### Performance

* ***:**  Update build script & add version provider ([0e8b77c4](0e8b77c4))
* **src/main/*:**
  *  Change subcommand version provider ([d5980123](d5980123))
  *  Rework EvaluateOperation ([ae755884](ae755884))
  *  Move data set preparation from the `Miner.class` ([53ffd990](53ffd990))
  *  Update data set split code ([825a1017](825a1017))
* **src/main/*/Cli.java:**  Add a sort operation for help output ([2a2e1252](2a2e1252))
* **src/main/*/MineOperation.java:**  Remove odd/even sequence generation ([9c154770](9c154770))
* **src/main/java*:**  Rework test data source specification ([83d9b26d](83d9b26d))

#### Features

* ***:**
  *  Add model evaluation subcommand ([01dc80e4](01dc80e4))
  *  Add tool list generation ([5698c974](5698c974))
  *  Update the code ([18a373e9](18a373e9))
* **Src/main/*:**  Add classifier,clusterer,filter help generation ([229f9f6a](229f9f6a))
* **src/main/*:**  Rename classes & add sercher, evaluator help ([ea2b50ed](ea2b50ed))

#### Bug Fixes

* ***:**
  *  Suppress gradle build warings ([5926d0ef](5926d0ef))
  *  Fix issue setting tool options ([bfd982d6](bfd982d6))
* **.gitignore:**  Readd JDTLS ignores ([4a793565](4a793565))
* **src/main/*:**
  *  Fix issues & remove dead code ([836568b8](836568b8))
  *  Sort out metric gather exception & prediction save ([caec1344](caec1344))
