## To Do

- Rework attribute selector configuration.
    > Provide for configurable attribute selection evaluators & searchers.
    * [x] Provide help output for evaluators & searchers.
    * [x] Add parser for attribute selection parameters.
    * [ ] Test attribute selection option parser.
- Document the code.
- Add tests.
- Rework file names for models & evaluation results.
    > Alter filename generation to include the filters applied to a data set in a sane way.
    * [x] Add applied filters to the model & result filenames.
    * [ ] Make the file names sane.

## Done

- Rework filter, classifier & clusterer option configuration.
    > Replace the baked in defaults with the possibility of user specified options, valid for the usage context. Referring to classifiers, clusterers & filters collectively as tools.
    * [x] Provide help output for tool options by an option taking the tool name as a parameter.
    * [x] Provide a means to specify tool options: <tool-name>:<option-1>:<option-2>...<option-n>.
    * [x] Add code to parse tool options and set the specified options.
    * [x] Remove redefinition of default classifier options.
    * [x] Remove redefinition of default clusterer options.
    * [x] Remove redefinition of default filter options.
- Generate lists of all classifiers, clusterers & filters.
    > Using the reflections library, get subclasses of AbstractClassifer, AbstractClusterer, Filters, Evaluators & Searchers to populate the list of all of the repective tools.
    * [x] Configure list generation for classifiers.
    * [x] Configure list generation for clusterers.
    * [x] Configure list generation for filters.
    * [x] Configure list generation for evaluators & searchers.
- Fix reflections manifest loading.
    > The reflections manifest is saved but leading it is another issue.
- Fix issue with classifier, clusterer instantiation.
    > Since moving from the hard coded classifier &  clusterer options, attempts to instantiate either classifiers or clusterers fails with an "Illegal options" error
    * [x] Identify reason for error
    * [x] Fix/work around the problem
- Save the output of `clusterResultsToString` as the prediction output.
    * [x] Add operations to save output
    * [x] Test output saving
