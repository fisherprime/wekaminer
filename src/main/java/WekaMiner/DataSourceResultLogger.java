// SPDX-License-Identifier: MIT

package WekaMiner;

import java.time.LocalDateTime;

/**
 * This class implements operations to distinguish the results log entries for
 * different data sources.
 * */
public class DataSourceResultLogger {
	private boolean firstDataSet;

	private final int LOG_DELIMITER_LENGTH = 50;

	private String resultLogPath;
	private String dataSourcePath;

	private final String LOG_DELIMITER = Utils.stringRepeat("=", LOG_DELIMITER_LENGTH);
	private final String LOG_DELIMITER_FORMAT = LOG_DELIMITER + " \n%s \n" + LOG_DELIMITER + "\n";

	public DataSourceResultLogger(String resultLogPath, String dataSourcePath, boolean firstDataSet) {
		this.dataSourcePath = dataSourcePath;
		this.firstDataSet = firstDataSet;
		this.resultLogPath = resultLogPath;
	}

	public void
	beginLogging() throws Exception {
		Utils.logInfo("Absolute data source path: " + dataSourcePath);

		String start = String.format(LOG_DELIMITER_FORMAT, "File: " +
		                             dataSourcePath + "\nStart date-time: " +
		                             LocalDateTime.now());

		if (!firstDataSet) {
			Utils.updateResultLog(resultLogPath, "\n\n" + start);

			return;
		}

		Utils.updateResultLog(resultLogPath, start);
	}

	public void
	endLogging() throws Exception {
		String stop = String.format(LOG_DELIMITER_FORMAT,  "File: " +
		                            dataSourcePath + "\nStop date-time: " +
		                            LocalDateTime.now());

		Utils.updateResultLog(resultLogPath, stop);
	}
}
