// SPDX-License-Identifier: MIT

package WekaMiner;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import weka.core.OptionHandler;

public class Cli {
	public static final String WEKA_TOOLS = "@|fg(yellow) Weka|@ classifier, clusterer, filters, evaluators or searchers";

	public static class CommonOptions {
		@Option(names = {"-r", "--root-dir"},
		        description = "Directory containing the data set(s) to work on.")
		String rootDir = "";

		@Option(names = {"-o", "--results-log"}, paramLabel = "FILE",
		        defaultValue = "results.log",
		        description = "File to log data mining results (path relative to" +
		                      " @|bold --root-dir|@).%n" +
		                      "Default: @|fg(green) ${DEFAULT-VALUE}|@.")
		String resultLogPath;

		static String cliArguments;

		public boolean
		validateRootDir() throws IllegalArgumentException {
			if (!Validator.validatePathExists(rootDir))
				throw new IllegalArgumentException(
					String.format("The defined root directory does not exist: \"%s\".", rootDir));

			return true;
		}

		public boolean
		validateResultLogPath() throws Exception {
			if (resultLogPath.isBlank())
				throw new Exception("The result log file has not been specified.");

			ArrayList < String > resultLogSplit = Utils.splitFilenameExt(resultLogPath);
			if (resultLogSplit.size() == 1)
				resultLogSplit.add("log");

			resultLogPath = Validator.validateSaveFileName(
				rootDir + "/" + resultLogSplit.get(0), resultLogSplit.get(1));

			return true;
		}
	}

	@Command(name = "list", aliases = "ls",
	         versionProvider = ManifestVersionProvider.class,
	         usageHelpAutoWidth = true, mixinStandardHelpOptions = true,
	         optionListHeading = "%nOptions:%n",
	         description = "Generate a list of available " + WEKA_TOOLS + ".")
	public static class ListOperation {
		@Command(name = "classifiers",
		         description = "Generate a list of available Weka classifiers.")
		public void
		classifiers() {
			generateToolList(App.WekaType.CLASSIFIER);
		}

		@Command(name = "clusterers",
		         description = "Generate a list of available Weka clusterers.")
		public void
		clusterers() {
			generateToolList(App.WekaType.CLUSTERER);
		}

		@Command(name = "filters",
		         description = "Generate a list of available Weka filters.")
		public void
		filters() {
			generateToolList(App.WekaType.FILTER);
		}

		@Command(name = "evaluators",
		         description = "Generate a list of available Weka evaluators.")
		public void
		evaluators() {
			generateToolList(App.WekaType.EVALUATOR);
		}

		@Command(name = "searchers",
		         description = "Generate a list of available Weka searchers.")
		public void
		searchers() {
			generateToolList(App.WekaType.EVALUATOR);
		}

		/**
		 * REF: Reflection invokation https://stackoverflow.com/a/161005.
		 * REF: Reflect class https://stackoverflow.com/a/2127384.
		 */
		private void
		generateToolList(App.WekaType toolType) {
			String type = "";

			StringBuffer helpOutput = new StringBuffer();

			ArrayList < String > completeToolList = new ArrayList < String > ();

			switch (toolType) {
				case CLASSIFIER:
					type = "classifiers";
					helpOutput.append("Classifiers:");

					completeToolList = Finder.getClassifiers(0);

					break;
				case CLUSTERER:
					type = "clusterers";
					helpOutput.append("Clusterers:");

					completeToolList = Finder.getClusterers(0);

					break;
				case FILTER:
					type = "filters";
					helpOutput.append("Filters:");

					completeToolList = Finder.getFilters(0);

					break;
				case EVALUATOR:
					type = "evaluators";
					helpOutput.append("Evaluators:");

					completeToolList = Finder.getEvaluators(0);

					break;
				case SEARCHER:
					type = "searchers";
					helpOutput.append("Searchers:");

					completeToolList = Finder.getSearchers(0);

					break;
				default:
					return;
			}

			if (completeToolList.isEmpty()) {
				Utils.logWarn("Could not generate a list of " + type + ".");

				return;
			}

			helpOutput.append("\nThe last segment --dot-separated-- of the class name is mandatory.\n");

			Collections.sort(completeToolList);
			for (String tool: completeToolList)
				helpOutput.append("\n" + tool);

			Utils.logInfo(helpOutput.toString());
		}
	}

	@Command(name = "info",
	         versionProvider = ManifestVersionProvider.class,
	         usageHelpAutoWidth = true, mixinStandardHelpOptions = true,
	         optionListHeading = "%nOptions:%n",
	         description = "Generate the usage options for a list of space-separated " + WEKA_TOOLS + ".")
	public static class InfoOperation implements Callable < Integer > {
		@Parameters(paramLabel = "TOOL", arity = "1..*",
		            description = WEKA_TOOLS + " to get information about.")
		private ArrayList < String > toolList;

		private final int INFO_DELIMITER_LENGTH = 50;
		private final String INFO_DELIMITER = "\n" + Utils.stringRepeat("=", INFO_DELIMITER_LENGTH);

		private void
		generateToolHelp(String tool) throws IllegalArgumentException {
			// Find a scheme that matches the supplied suffix.
			List < String > matches = Finder.getWekaMatches(tool);

			if (matches.size() > 1)
				Utils.logInfo(
					String.format("(%s) matches multiple schemes all available help will be presented.",
					              tool));

			for (String match: matches) {
				try {
					StringBuffer helpOutput = new StringBuffer();

					Class < ? > toolClass = Class.forName(match);
					Object classInstance = toolClass.getDeclaredConstructor().newInstance();

					Method globalInfoMethod = toolClass.getMethod("globalInfo");

					((OptionHandler) classInstance).listOptions().asIterator()
					.forEachRemaining(toolOption->{
						helpOutput.append(
							String.format("\n-%s %s\n", toolOption.name(),
							              toolOption.description()));
					});

					helpOutput.append("\n" + globalInfoMethod.invoke(classInstance));

					System.out.println(INFO_DELIMITER);
					Utils.logInfo(String.format("(%s): \n%s", match, helpOutput.toString()));
				} catch(Exception e) {
					Utils.logWarn("Error occured while generating help: " + e);
				}
			}
		}

		@Override
		public Integer
		call() {
			try {
				for (String tool : toolList) {
					try {
						generateToolHelp(tool);
					} catch(Exception e) {
						Utils.logWarn(e.getMessage());
					}
				}

				return 0;
			} catch(Exception e) {
				Utils.logWarn(e.getMessage());

				return 1;
			}
		}
	}
}
