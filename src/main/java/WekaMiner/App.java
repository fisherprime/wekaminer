// SPDX-License-Identifier: MIT

/*
 * Run Weka classifiers & clusterers on data sets.
 */
package WekaMiner;

import java.util.concurrent.Callable;

import picocli.AutoComplete;
import picocli.CommandLine;
import picocli.CommandLine.Command;

@Command(name = "WekaMiner",
         versionProvider = ManifestVersionProvider.class,
         usageHelpAutoWidth = true, mixinStandardHelpOptions = true,
         commandListHeading = "%nCommands:%n",
         descriptionHeading = "%nDescription:%n%n",
         optionListHeading = "%nOptions:%n",
         synopsisHeading = "%n",
         subcommands = {AutoComplete.GenerateCompletion.class,
                        MineOperation.class, EvaluateOperation.class,
                        ConvertOperation.class, Cli.ListOperation.class, Cli.InfoOperation.class},
         description = "This tool provides facilities to filter, select" +
                       " attributes & split user supplied data sets. The" +
                       " prepared data sets are classified/clustered in parallel" +
                       " (@|bold parallelism = numCPUs|@)," +
                       " cross-validatio/testing performed as desired by the" +
                       " user & the results output to a data source specific" +
                       " directory. A log for the performed operations is saved" +
                       " at @|bold --results-log|@.")
public class App implements Callable < Void > {
	public static boolean DEBUG_MODE = true;

	public static final String ARFF_EXTENSION = "arff";
	public static final String MODEL_EXTENSION = "model";
	public static final String CSV_EXTENSION = "csv";
	public static final String LOG_EXTENSION = "log";

	public static enum WekaType {
		CLASSIFIER, CLUSTERER, EVALUATOR, FILTER, SEARCHER,
		ATTRIBUTE_SELECTOR;
	}

	public static String[] cliArguments;

	@Override
	public Void
	call() {
		Utils.logWarn("Specify a subcommand to execute");

		return null;
	}

	public static void
	main(String[] args) {
		if (args.length == 0) {
			new CommandLine(new App())
			.setUsageHelpLongOptionsMaxWidth(30)
			.usage(System.out);

			System.exit(0);
		}

		cliArguments = args;

		System.exit(new CommandLine(new App())
		            .setUsageHelpLongOptionsMaxWidth(30)
		            .setAbbreviatedSubcommandsAllowed(true)
		            .setAbbreviatedOptionsAllowed(true).execute(args));
	}
}
