// SPDX-License-Identifier: MIT

package WekaMiner;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.clusterers.AbstractClusterer;
import weka.clusterers.ClusterEvaluation;
import weka.clusterers.Clusterer;
import weka.clusterers.MakeDensityBasedClusterer;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

public class Miner extends Thread {
	private boolean crossValidateOption = false;
	private boolean saveModelOption = false;

	private App.WekaType minerType;

	// TODO: Look into changing this option to a parameter.
	private final int crossValidateFolds = 10;
	private int neighboursClusters = 0;

	private long miningTimerStart = 0;
	private long evaluationTimerStart = 0;

	private double crossValidationLogLikelihood;

	private String dataSourceBaseName;
	private String minerName;
	private String modelBaseName = "";
	private String modelSaveDir;
	private String predictionsResultsSaveDir;
	private String preparationsInfo = "";
	private String resultLogPath;
	private String rootDir;

	private StringBuffer operationInfo;

	private String[] minerOptions;

	private Classifier classifier;
	private Clusterer clusterer;

	private Evaluation classifierEvaluation;
	private ClusterEvaluation clusterEvaluation;

	private Instances evaluationDataSet;
	private Instances trainDataSet;
	private Instances testDataSet;

	private ResultBuffer resultBuffer;

	/**
	 * The mining operation constructor.
	 */
	public
	Miner(String dataMinerName, String arguments, Instances[] dataSets) throws Exception {
		minerName = dataMinerName;
		minerOptions = weka.core.Utils.splitOptions(arguments);

		if ((dataSets[0] != null) && !dataSets[0].isEmpty())
			trainDataSet = dataSets[0];

		if ((dataSets[1] != null) && !dataSets[1].isEmpty())
			testDataSet = dataSets[1];

		if ((dataSets[2] != null) && !dataSets[2].isEmpty())
			evaluationDataSet = dataSets[2];

		instantiateMiner();
	}

	/**
	 * The model evaluation constructor.
	 */
	public
	Miner(Object model, App.WekaType minerType, String modelBaseName, Instances dataSet) throws Exception {
		switch (minerType) {
			case CLASSIFIER:
				classifier = AbstractClassifier.makeCopy((Classifier) model);
				break;
			case CLUSTERER:
				clusterer = AbstractClusterer.makeCopy((Clusterer) model);
				break;
			default:
				throw new IllegalArgumentException(
					String.format("(%s) is neither a classifier nor a clusterer model.",
					              modelBaseName));
		}

		this.modelBaseName = modelBaseName;
		testDataSet = dataSet;
	}

	public void
	instantiateMiner() throws Exception {
		for (String match: Finder.getWekaMatches(minerName)) {
			Class < ? > minerClass = Class.forName(match);

			if (AbstractClassifier.class.isAssignableFrom(minerClass)) {
				try {
					classifier = AbstractClassifier.forName(minerName, minerOptions);
					minerType = App.WekaType.CLASSIFIER;

					return;
				} catch(Exception e) {
					throw new IllegalArgumentException(
						String.format("%s classifier instantiation failed: %s",
						              minerName, e));
				}
			}

			if (AbstractClusterer.class.isAssignableFrom(minerClass)) {
				try {
					clusterer = AbstractClusterer.forName(minerName, minerOptions);
					minerType = App.WekaType.CLUSTERER;

					return;
				} catch(Exception e) {
					throw new IllegalArgumentException(
						String.format("%s clusterer instantiation failed: %s",
						              minerName, e));
				}
			}

			// NOTE: Should not occur.
			throw new IllegalArgumentException(
				String.format("%s is neither a classifier nor a clusterer.", minerName));
		}
	}

	public void
	setPaths(String rootPath, String dataSourceBaseName, String resultLogPath) throws Exception {
		rootDir = rootPath;
		this.resultLogPath = resultLogPath;
		this.dataSourceBaseName = dataSourceBaseName;

		if (minerType == App.WekaType.CLASSIFIER) {
			if (!modelBaseName.isEmpty()) {
				predictionsResultsSaveDir = String.format("%s/classifier-model-predictions/%s", rootDir, dataSourceBaseName);
				Utils.createDirectory(predictionsResultsSaveDir);

				return;
			}

			modelSaveDir = String.format("%s/classifier-models/%s", rootDir, dataSourceBaseName);
			Utils.createDirectory(modelSaveDir);

			predictionsResultsSaveDir = String.format("%s/classifier-predictions/%s", rootDir, dataSourceBaseName);
			Utils.createDirectory(predictionsResultsSaveDir);

			return;
		}

		if (!modelBaseName.isEmpty()) {
			predictionsResultsSaveDir = String.format("%s/cluster-model-results/%s", rootDir, dataSourceBaseName);
			Utils.createDirectory(predictionsResultsSaveDir);

			return;
		}

		modelSaveDir = String.format("%s/clusterer-models/%s", rootDir, dataSourceBaseName);
		Utils.createDirectory(modelSaveDir);

		predictionsResultsSaveDir = String.format("%s/cluster-results/%s", rootDir, dataSourceBaseName);
		Utils.createDirectory(predictionsResultsSaveDir);
	}

	public void
	setOperationInfo(String info) {
		this.operationInfo = new StringBuffer(info);
	}

	public void
	setPreparationsInfo(String info) {
		this.preparationsInfo = info;
	}

	public void
	setCrossValidate(boolean opt) {
		crossValidateOption = opt;
	}

	public void
	setSaveModel(boolean opt) {
		saveModelOption = opt;
	}

	public void
	setNeighboursClusters(int value) {
		neighboursClusters = value;
	}

	private void
	buildClassifier() throws Exception {
		try {
			classifier.getCapabilities().testWithFail(trainDataSet);
		} catch(Exception e) {
			throw new IllegalArgumentException(
				String.format("Cannot handle the data set for {%s}: %s",
				              operationInfo, e));
		}

		Utils.logInfo("Building classifier for: " + operationInfo);

		miningTimerStart = Utils.now();
		classifier.buildClassifier(trainDataSet);

		// resultBuffer.addNewLine();
		resultBuffer.addLine(
			String.format("[Classifier build time]: %ds",
			              Utils.getDifferenceFromNow(miningTimerStart,
			                                         TimeUnit.SECONDS)), 1);
	}

	private void
	buildClusterer() throws Exception {
		try {
			trainDataSet = removeClassAttribute(trainDataSet);
			clusterer.getCapabilities().testWithFail(trainDataSet);
		} catch(Exception e) {
			throw new IllegalArgumentException(
				String.format("Cannot handle the data set for {%s}: %s",
				              operationInfo, e));
		}

		if (testDataSet != null)
			testDataSet = removeClassAttribute(testDataSet);

		if (evaluationDataSet != null)
			evaluationDataSet = removeClassAttribute(evaluationDataSet);

		Utils.logInfo("Building clusterer for: " + operationInfo);

		miningTimerStart = Utils.now();
		clusterer.buildClusterer(trainDataSet);

		// resultBuffer.addNewLine();
		resultBuffer.addLine(
			String.format("[Clusterer build time]: %ds",
			              Utils.getDifferenceFromNow(miningTimerStart,
			                                         TimeUnit.SECONDS)), 1);
	}

	/**
	 * This method assumes the last attribute is the class attribute.
	 */
	public Instances
	removeClassAttribute(Instances dataSet) throws Exception {
		// Do nothing if the class attribute is not set.
		if (dataSet.classIndex() == -1)
			return dataSet;

		Remove filter = new Remove();

		// NOTE: The indices specified here are indexed from `1`.
		filter.setAttributeIndices("" + (dataSet.classIndex() + 1));
		filter.setInputFormat(dataSet);

		return Filter.useFilter(dataSet, filter);
	}

	private String
	generateFilenamePrefix() {
		if (!modelBaseName.isEmpty())
			return dataSourceBaseName + "-eval-" + modelBaseName;

		StringBuffer prefix = new StringBuffer(dataSourceBaseName + "-" + minerName);

		if (neighboursClusters != 0)
			prefix.append("-").append(neighboursClusters);

		if (!preparationsInfo.isBlank())
			prefix.append(preparationsInfo);

		if (crossValidateOption)
			prefix.append("-cross-validate");

		return prefix.toString();
	}

	private void
	saveModel() throws Exception {
		Utils.logInfo("Saving model for: " + operationInfo);

		String savePath;
		StringBuffer savePathBuffer = new StringBuffer();

		savePathBuffer.append(modelSaveDir + "/" +
		                      generateFilenamePrefix());
		savePath = Validator.validateSaveFileName(savePathBuffer.toString(), App.MODEL_EXTENSION);

		// resultBuffer.addNewLine();

		if (minerType == App.WekaType.CLASSIFIER) {
			weka.core.SerializationHelper.write(savePath, classifier);
			resultBuffer.addLine("[Classifier model save path]: " + savePath, 1);

			return;
		}

		weka.core.SerializationHelper.write(savePath, clusterer);
		resultBuffer.addLine("[Clusterer model save path]: " + savePath, 1);
	}

	private void
	logClassifierEvaluation(String dataSetType) {
		// NOTE: Avoiding `divide-by-zero` cases when nothing is classified
		// correctly.
		boolean divideByZero = classifierEvaluation.pctCorrect() == 0;

		int numClasses;

		double rocValue = 0;
		double rocSum = 0;

		// resultBuffer.addNewLine();

		if (dataSetType.equals("cross-validate"))
			resultBuffer.addLine(String.format("[Cross-validation summary]: {", dataSetType), 1);
		else
			resultBuffer.addLine(String.format("[Evaluate %s data set summary]: {", dataSetType), 1);

		resultBuffer.addLine(String.format("[Time elapsed]: %ds",
		                                   Utils.getDifferenceFromNow(evaluationTimerStart, TimeUnit.SECONDS)), 2);

		resultBuffer.addLine("[Accuracy]: " + classifierEvaluation.pctCorrect() + "%", 2);

		if (!divideByZero)
			resultBuffer.addLine("[Weighted F-measure]: " + classifierEvaluation.weightedFMeasure(), 2);

		resultBuffer.addLine("[Instances]: " + classifierEvaluation.numInstances(), 2);
		resultBuffer.addLine("[RMS]: " + classifierEvaluation.rootMeanSquaredError(), 2);

		if (!divideByZero) {
			numClasses = classifierEvaluation.getClassPriors().length;

			for (int iter = 0; iter < numClasses; ++iter) {
				rocValue = classifierEvaluation.areaUnderROC(iter);
				rocSum += rocValue;
				resultBuffer.addLine("[ROC](" + iter + "): " + rocValue, 2);
			}

			resultBuffer.addLine("[ROC](average): " + (rocSum / numClasses), 2);
		}

		try {
			resultBuffer.addLine("[Confusion matrix]:\n" + classifierEvaluation.toMatrixString(), 2);
		} catch(Exception e) {
			// Do nothing on exception.
		}

		resultBuffer.addLine("}", 1);
	}

	private void
	logClustererEvaluation(String dataSetType) {
		// resultBuffer.addNewLine();

		if (dataSetType.equals("cross-validate"))
			resultBuffer.addLine(String.format("[Cross-validation summary]: {", dataSetType), 1);
		else
			resultBuffer.addLine(String.format("[Evaluate %s data set summary]: {", dataSetType), 1);

		resultBuffer.addLine("[Number of clusters]: " + clusterEvaluation.getNumClusters(), 2);
		if (dataSetType.equals("Cross validation"))
			resultBuffer.addLine("[Log-likelihood]: " + crossValidationLogLikelihood, 2);
		// resultBuffer.addResultLine("[Results]:\n" +
		// clustererEvaluation.clusterResultsToString(), 2);

		resultBuffer.addLine("}", 1);
	}

	private String
	generateEvaluationResultsFileName(int dataSetMask) {
		StringBuffer savePathBuffer = new StringBuffer();

		savePathBuffer.append(predictionsResultsSaveDir + "/" +
		                      generateFilenamePrefix());

		if (modelBaseName.isEmpty()) {
			switch (dataSetMask) {
				case DataSetPreparator.TEST_MASK:
					savePathBuffer.append("-test");
					break;
				case DataSetPreparator.EVALUATE_MASK:
					savePathBuffer.append("-evaluate");
					break;
				default:
			}
		}

		if (minerType == App.WekaType.CLASSIFIER)
			return Validator.validateSaveFileName(savePathBuffer.toString(), App.CSV_EXTENSION);

		return Validator.validateSaveFileName(savePathBuffer.toString(), App.LOG_EXTENSION);
	}

	private void
	saveClassifierPredictions(List < Prediction > predictions, int dataSetMask) {
		Utils.logInfo("Saving classifier predictions for: " + operationInfo);

		String savePath = generateEvaluationResultsFileName(dataSetMask);

		Utils.writePredictionsToCsv(predictions, savePath);

		// resultBuffer.addNewLine();
		resultBuffer.addLine("[Classifier predictions save path]: " + savePath, 1);
	}

	private void
	saveClusterResults(String clusterResults, int dataSetMask) {
		try {
			Utils.logInfo("Saving cluster results for: " + operationInfo);

			String savePath = generateEvaluationResultsFileName(dataSetMask);
			BufferedWriter writer = new BufferedWriter(new FileWriter(savePath, true));

			writer.write(clusterResults);
			writer.flush();
			writer.close();

			// resultBuffer.addNewLine();
			resultBuffer.addLine("[Cluster results save path]: " + savePath, 1);
		} catch(IOException io) {
			Utils.logWarn(io.getMessage());
		}
	}

	/**
	 * This operation incrementally trains and evaluates a model for N
	 * iterations.
	 */
	private void
	crossValidateClassifier() throws Exception {
		Utils.logInfo("Cross validating for: " + operationInfo);

		evaluationTimerStart = Utils.now();
		classifierEvaluation = new Evaluation(trainDataSet);

		classifierEvaluation.crossValidateModel(classifier, trainDataSet,
		                                        crossValidateFolds, new Random(1));

		logClassifierEvaluation("cross-validate");
		saveClassifierPredictions(classifierEvaluation.predictions(), 0);
	}

	/**
	 * This operation evaluates a built model with some test/evaluate x data
	 * set.
	 */
	private void
	evaluateClassifier(int dataSetMask) throws Exception {
		if (dataSetMask == DataSetPreparator.TEST_MASK)
			Utils.logInfo("Evaluating classifier model (test data set) for: " +
			              operationInfo);
		else if (dataSetMask == DataSetPreparator.EVALUATE_MASK)
			Utils.logInfo("Evaluating classifier model (evaluation data set) for: " +
			              operationInfo);

		evaluationTimerStart = Utils.now();
		classifierEvaluation = new Evaluation(trainDataSet);

		if (dataSetMask == DataSetPreparator.TEST_MASK) {
			classifierEvaluation.evaluateModel(classifier, testDataSet);
			logClassifierEvaluation("test");
		} else if (dataSetMask == DataSetPreparator.EVALUATE_MASK) {
			classifierEvaluation.evaluateModel(classifier, evaluationDataSet);
			logClassifierEvaluation("evaluation");
		} else {
			// Should not occur.
		}

		saveClassifierPredictions(classifierEvaluation.predictions(),
		                          dataSetMask);
	}

	// NOTE: This method is messy.
	// NOTE: Cross validation works for `DensityBasedClusters`.
	private void
	crossValidateClusterer() throws Exception {
		Utils.logInfo("Cross validating for: " + operationInfo);

		evaluationTimerStart = Utils.now();
		clusterEvaluation = new ClusterEvaluation();

		crossValidationLogLikelihood = ClusterEvaluation.crossValidateModel(
			new MakeDensityBasedClusterer(clusterer), trainDataSet,
			crossValidateFolds, new Random(1));

		logClustererEvaluation("cross-validate");
		saveClusterResults(clusterEvaluation.clusterResultsToString(), 0);
	}

	private void
	evaluateClusterer(int dataSetMask) throws Exception {
		if (dataSetMask == DataSetPreparator.TEST_MASK)
			Utils.logInfo("Evaluating clusterer model (test data set) for: " +
			              operationInfo);
		else if (dataSetMask == DataSetPreparator.EVALUATE_MASK)
			Utils.logInfo("Evaluating clusterer model (evaluation data set) for: " +
			              operationInfo);

		evaluationTimerStart = Utils.now();
		clusterEvaluation = new ClusterEvaluation();

		clusterEvaluation.setClusterer(clusterer);

		if (dataSetMask == DataSetPreparator.TEST_MASK) {
			clusterEvaluation.evaluateClusterer(testDataSet);
			logClustererEvaluation("test");
		} else if (dataSetMask == DataSetPreparator.EVALUATE_MASK) {
			clusterEvaluation.evaluateClusterer(evaluationDataSet);
			logClustererEvaluation("evaluation");
		} else {
			// Should not occur.
		}

		saveClusterResults(clusterEvaluation.clusterResultsToString(),
		                   dataSetMask);
	}

	private void
	classifyAndEvaluate() throws Exception {
		Utils.logInfo("Classifying: " + operationInfo);

		buildClassifier();

		if (saveModelOption)
			saveModel();

		if (crossValidateOption)
			crossValidateClassifier();
		else if (testDataSet != null)
			evaluateClassifier(DataSetPreparator.TEST_MASK);

		if (evaluationDataSet != null)
			evaluateClassifier(DataSetPreparator.EVALUATE_MASK);
	}

	private void
	clusterAndEvaluate() throws Exception {
		Utils.logInfo("Clustering: " + operationInfo);

		buildClusterer();

		if (saveModelOption)
			saveModel();

		if (crossValidateOption)
			crossValidateClusterer();
		else if (testDataSet != null)
			evaluateClusterer(DataSetPreparator.TEST_MASK);

		if (evaluationDataSet != null)
			evaluateClusterer(DataSetPreparator.EVALUATE_MASK);
	}

	@Override
	public void
	run() {
		try {
			resultBuffer = new ResultBuffer(resultLogPath, minerType);
			resultBuffer.beginBuffering();

			if (minerType == App.WekaType.CLASSIFIER) {
				resultBuffer.addLine("[Classification operation info]: " + operationInfo, 1);

				if (!modelBaseName.isEmpty()) {
					miningTimerStart = Utils.now();
					evaluateClassifier(DataSetPreparator.TEST_MASK);
				} else {
					classifyAndEvaluate();
				}
			} else {
				resultBuffer.addLine("[Clustering operation info]: " + operationInfo, 1);

				if (!modelBaseName.isEmpty()) {
					miningTimerStart = Utils.now();
					evaluateClusterer(DataSetPreparator.TEST_MASK);
				} else {
					clusterAndEvaluate();
				}
			}

			// resultBuffer.addNewLine();
			resultBuffer.addLine(
				String.format("[Elapsed time]: %ds",
				              Utils.getDifferenceFromNow(miningTimerStart,
				                                         TimeUnit.SECONDS)), 1);

			resultBuffer.endBuffering();
		} catch(Exception e) {
			resultBuffer = new ResultBuffer(resultLogPath, minerType);
			resultBuffer.beginBuffering();

			if (minerType == App.WekaType.CLASSIFIER)
				resultBuffer.addLine("[Classification operation info]: " + operationInfo, 1);
			else
				resultBuffer.addLine("[Clustering operation info]: " + operationInfo, 1);

			resultBuffer.addLine("[Error]: " + e, 1);
			resultBuffer.endBuffering();;

			Utils.logWarn(
				String.format("Data mining for {%s} failed: \n\t%s.", operationInfo, e));
		}
	}
}
