// SPDX-License-Identifier: MIT

package WekaMiner;

/**
 * This class implements a buffer for the results log's contents.
 */
public class ResultBuffer {
	private App.WekaType taskMask;

	private String resultLogPath;

	private StringBuffer buffer;

	public ResultBuffer(String resultLogPath, App.WekaType ... taskMask) {
		this.resultLogPath = resultLogPath;
		if ((taskMask != null) && (taskMask.length == 1))
			this.taskMask = taskMask[0];
	}

	public void
	addNewLine() {
		buffer.append("\n");
	}

	public void
	addLine(String message, int indentLevel) {
		for (int iter = 1; iter <= indentLevel; ++iter)
			buffer.append("\t");
		buffer.append(message + "\n");
	}

	public void
	beginBuffering() {
		if (taskMask != null)
			buffer = new StringBuffer("{\n");

		buffer = new StringBuffer();
		addNewLine();

		return;
	}

	public void
	endBuffering() {
		if (taskMask != null)
			addLine("}\n", 0);
		else
			addNewLine();

		Utils.updateResultLog(resultLogPath, buffer.toString());
	}
}
