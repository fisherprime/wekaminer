// SPDX-License-Identifier: MIT

package WekaMiner;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.attributeSelection.AttributeSelection;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.instance.RemovePercentage;

public class DataSetPreparator {
	private boolean splitsOverlap = false;

	// Data set type bitmasks.
	public static final int TEST_MASK = 0b00000001;
	public static final int EVALUATE_MASK = 0b00000010;
	private int dataSplitTypes = 0;

	private Integer[] dataSetSplitPercentages;

	private String resultLogPath;
	private String filterName;

	private StringBuffer operationInfo;
	private StringBuffer preparationsInfo;

	private AttributeSelection attributeSelector;
	private ASEvaluation evaluator;
	private ASSearch searcher;

	private Filter filter;

	private Instances dataSet;

	public DataSetPreparator(Instances dataSet, String resultLogPath,
	                         StringBuffer operation, StringBuffer preparations) {
		this.dataSet = dataSet;
		this.operationInfo = operation;
		this.resultLogPath = resultLogPath;
		this.preparationsInfo = preparations;
	}

	public String
	getOperationInfo() {
		return operationInfo.toString();
	}

	public void
	configureFilter(String filterWithOptions) throws Exception {
		List < String > matches;
		ArrayList < String > splitOptions = Utils.splitToolWithOptions(filterWithOptions);

		filterName = splitOptions.get(0);
		splitOptions.remove(0);

		matches = Finder.getWekaMatches(filterName);

		// NOTE: Only one match should be returned, there is no overlap
		// between the supervised/unsupervised and attribute/instance filters.
		filter = (Filter) Class.forName(matches.get(0))
		         .getDeclaredConstructor().newInstance();

		filter.setOptions(splitOptions.toArray(new String[0]));
		filter.setInputFormat(dataSet);
	}

	// TODO: Validate.
	public void
	configureAttributeSelector(String[] evaluatorSearcher) throws IllegalArgumentException {
		try {
			String evaluatorName;
			String searcherName;

			ArrayList < String > evaluatorSplitOption = Utils.splitToolWithOptions(evaluatorSearcher[0]);
			ArrayList < String > searcherSplitOption = Utils.splitToolWithOptions(evaluatorSearcher[1]);

			evaluatorName = evaluatorSplitOption.get(0);
			evaluatorSplitOption.remove(0);

			searcherName = searcherSplitOption.get(0);
			searcherSplitOption.remove(0);

			evaluator = ASEvaluation.forName(evaluatorName, evaluatorSplitOption.toArray(new String[0]));
			searcher = ASSearch.forName(searcherName, searcherSplitOption.toArray(new String[0]));

			attributeSelector = new AttributeSelection();
		} catch(Exception e) {
			throw new IllegalArgumentException("Could not configure the attribute selector: " + e);
		}
	}

	/**
	 * This operation splits a data set into the train/split/evaluation
	 * data sets, the test data set isn't created for cross-validation
	 */
	public void
	configureDataSetSplitter(Integer[] splitPercentages) throws Exception {
		int sum = 0;

		ArrayList < Integer > splitPercentagesList = new ArrayList < Integer > ();

		for (int split: splitPercentages)
			sum += split;

		if (sum > 100)
			splitsOverlap = true;

		splitPercentagesList.add(splitPercentages[0]);
		splitPercentagesList.add(splitPercentages[1]);

		if (splitPercentages[1] > 0)
			dataSplitTypes |= TEST_MASK;

		if (splitPercentages.length == 3) {
			dataSplitTypes |= EVALUATE_MASK;
			splitPercentagesList.add(splitPercentages[2]);
		}

		dataSetSplitPercentages = splitPercentagesList.toArray(new Integer[0]);
	}

	/**
	 * This method uses the `Weka.attributeSelection.AttributeSelection`
	 * filter that selects attributes from a data set.
	 */
	public void
	selectDataSetAttributes() throws Exception {
		if (attributeSelector == null)
			throw new IllegalStateException("The attribute selector has not been configured.");

		Utils.logInfo("Selecting attributes for: " + operationInfo);

		int numAttributes;
		int counter = 1;

		int[] selectedAttributeIndices;

		ResultBuffer resultBuffer = new ResultBuffer(resultLogPath);

		resultBuffer.beginBuffering();
		resultBuffer.addLine("Attriute selection summary: {", 0);

		attributeSelector.setEvaluator(evaluator);
		attributeSelector.setSearch(searcher);
		attributeSelector.SelectAttributes(dataSet);

		selectedAttributeIndices = attributeSelector.selectedAttributes();
		numAttributes = selectedAttributeIndices.length;

		StringBuffer logEntryBuffer = new StringBuffer(String.format("Selected attributes (%d): [", numAttributes));

		for (int index : selectedAttributeIndices) {
			logEntryBuffer.append(dataSet.attribute(index).name());

			if (counter != numAttributes)
				logEntryBuffer.append(", ");

			++counter;
		}
		logEntryBuffer.append("]");

		// Reduce the dimensionality of a set of instances to include only
		// those attributes chosen by the last run of attribute selection.
		dataSet = attributeSelector.reduceDimensionality(dataSet);

		resultBuffer.addLine(logEntryBuffer.toString(), 1);
		resultBuffer.addLine("}", 0);
		resultBuffer.endBuffering();

		operationInfo.append("->AttributeSelection");
		preparationsInfo.append("-AttributeSelection");
	}

	public void
	filterDataSet() throws Exception {
		if (filter == null)
			throw new IllegalStateException("No filter has not been configured.");

		Utils.logInfo(
			String.format("Running filter (%s): %s", filterName, operationInfo.toString()));

		dataSet = Filter.useFilter(dataSet, filter);

		operationInfo.append("->" + filterName);
		preparationsInfo.append("-" + filterName);
	}

	/**
	 * This method splits a data set based on user specified train/test/split
	 * percentages.
	 * The splits to perform are governed by the `dataSplitType` bitmask.
	 */
	public Instances []
	splitDataSet() throws Exception {
		if (dataSetSplitPercentages == null)
			throw new IllegalStateException("The data set split percentages have not been set.");

		Utils.logInfo("Splitting data set for: " + operationInfo.toString());

		RemovePercentage splitter;

		Instances trainDataSet = null;
		Instances testDataSet = null;
		Instances evaluationDataSet = null;

		ResultBuffer resultBuffer = new ResultBuffer(resultLogPath);

		resultBuffer.beginBuffering();
		resultBuffer.addLine("Data set split summary: {", 0);

		if ((dataSplitTypes & EVALUATE_MASK) == EVALUATE_MASK) {
			dataSet.randomize(new Random());

			splitter = new RemovePercentage();
			splitter.setPercentage(dataSetSplitPercentages[2]);

			// The below line sets the structure of the data set; content
			// contained is not used.
			splitter.setInputFormat(dataSet);

			// Remove the inverse of the selected Instances as specified by
			// the call to `setPercentage`.
			splitter.setInvertSelection(true);

			evaluationDataSet = Filter.useFilter(dataSet, splitter);

			dataSplitTypes &= ~EVALUATE_MASK;
		}

		splitter = new RemovePercentage();
		splitter.setPercentage(dataSetSplitPercentages[0]);
		splitter.setInputFormat(dataSet);
		splitter.setInvertSelection(true);

		trainDataSet = Filter.useFilter(dataSet, splitter);

		if ((dataSplitTypes & TEST_MASK) == TEST_MASK) {
			// NOTE: Randomize the original data set should the splits
			// specified overlap.
			if (splitsOverlap)
				dataSet.randomize(new Random());

			// NOTE: Reusing the `RemovePercentage` filter resulted in the
			// original data set assigned to the `testDataSet` variable.
			splitter = new RemovePercentage();
			splitter.setPercentage(dataSetSplitPercentages[1]);
			splitter.setInputFormat(dataSet);
			splitter.setInvertSelection(true);

			testDataSet = Filter.useFilter(dataSet, splitter);

			dataSplitTypes &= ~TEST_MASK;
		}

		resultBuffer.addLine("Initial data set instances: " + dataSet.numInstances(),   1);
		resultBuffer.addLine("Train data set instances: " + trainDataSet.numInstances(), 1);

		if (testDataSet != null)
			resultBuffer.addLine("Test data set instances: " + testDataSet.numInstances(), 1);

		if (evaluationDataSet != null)
			resultBuffer.addLine("Evaluation data set instances: " +
			                     evaluationDataSet.numInstances(), 1);

		resultBuffer.addLine("}", 0);
		resultBuffer.endBuffering();

		return new Instances[] {trainDataSet, testDataSet, evaluationDataSet};
	}
}
