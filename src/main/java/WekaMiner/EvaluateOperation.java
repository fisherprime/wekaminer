// SPDX-License-Identifier: MIT

package WekaMiner;

import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import WekaMiner.Cli.CommonOptions;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import weka.classifiers.AbstractClassifier;
import weka.clusterers.AbstractClusterer;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

// NOTE: To use this subcommand, the models to evaluate need to be moved from
// the data-source-specific model save directories to a common (unified) one.
@Command(name = "evaluate-model",
         versionProvider = ManifestVersionProvider.class,
         usageHelpAutoWidth = true, mixinStandardHelpOptions = true,
         optionListHeading = "%nOptions:%n",
         description = "Loads & evaluates a Weka generated model with a test data set.")
public class EvaluateOperation implements Callable < Integer > {
	@Mixin
	CommonOptions common;

	@Option(names = {"-d", "--model-dir"},
	        defaultValue = "models",
	        description = "Directory containing the classifier and/or clusterer model(s) to evaluate" +
	                      " relative to @|bold --root-dir|@.%n" +
	                      "Default: @|fg(green) ${DEFAULT-VALUE}|@.")
	String modelDir;

	@Option(names = {"-m", "--models"}, split = ",", paramLabel = "MODEL",
	        description = "Model(s) to evaluate, relative to @|bold" +
	                      " --model-dir|@.")
	private ArrayList < String > modelNames;

	@Parameters(arity = "1..*", paramLabel = "FILE",
	            description = "File(s) containing the test data set, relative to @|bold" +
	                          " --root-dir|@.")
	private ArrayList < String > dataSourceFileNames;

	private ExecutorService executor;

	private void
	poolEvaluationTask(Object model, App.WekaType minerType, String modelBaseName,
	                   Instances dataSet, String dataSourceFileName,
	                   String operationInfo) throws Exception {
		Miner modelEvaluator = new Miner(model, minerType, modelBaseName, dataSet);

		modelEvaluator.setPaths(common.rootDir, dataSourceFileName, common.resultLogPath);
		modelEvaluator.setOperationInfo(operationInfo);

		executor.execute(modelEvaluator);
	}

	private App.WekaType
	validateModelType(Object model, String modelPath) throws IllegalArgumentException {
		if (model instanceof AbstractClusterer)
			return App.WekaType.CLUSTERER;

		if (model instanceof AbstractClassifier) {
			throw new IllegalArgumentException(
				String.format("NOT IMPLEMENTED: The classifier model at (%s) requires a training data set for evaluation",
				              modelPath));
			// return Miner.CLASSIFIER_MASK;
		}

		throw new IllegalArgumentException(
			String.format("The model at: %s is neither a classifier nor a clusterer",
			              modelPath));
	}

	private void
	runMultiThread() throws Exception {
		boolean firstDataSet = true;

		for (String dataSourceFileName : dataSourceFileNames) {
			String dataSourcePath = common.rootDir + "/" + dataSourceFileName;

			DataSourceResultLogger resultLogger = new DataSourceResultLogger(common.resultLogPath, dataSourcePath, firstDataSet);
			resultLogger.beginLogging();

			try {
				// Create an asynchronous task manager for each data
				// set.
				executor = Executors.newWorkStealingPool();

				Instances dataSet = new DataSource(dataSourcePath).getDataSet();

				Utils.logDataSetSummary(common.resultLogPath, dataSet.toSummaryString());

				// NOTE: The test data set should not contain a class
				// attribute.

				/* if (dataSet.classIndex() == -1)
				 *     dataSet.setClassIndex(dataSet.numAttributes() - 1); */

				for (String modelName : modelNames) {
					String modelPath = common.rootDir + "/" + modelDir + "/" + modelName;

					Object model = weka.core.SerializationHelper.read(modelPath);

					try {
						App.WekaType modelType = validateModelType(model, modelPath);

						String operationInfo = String.format("%s %s", dataSourceFileName, modelName);
						String modelBaseName = Utils.splitFilenameExt(modelName).get(0);

						poolEvaluationTask(model, modelType,
						                   modelBaseName, dataSet,
						                   dataSourceFileName,
						                   operationInfo);
					} catch(IllegalArgumentException ia) {
						modelNames.remove(modelName);
						Utils.logWarn(ia.getMessage());
					}
				}

				Utils.shutdownAndAwaitTermination(executor);
			} catch(Exception e) {
				if (executor != null)
					executor.shutdownNow();

				Utils.updateResultLog(common.resultLogPath,
				                      "\nModel evaluation failed for the data ssource at: " + dataSourcePath + "\n\n");
				Utils.logWarn(e.getMessage());
			}

			resultLogger.endLogging();

			firstDataSet = false;
		}
	}

	@Override
	public Integer
	call() {
		try {
			common.validateRootDir();
			common.validateResultLogPath();

			if ((modelNames.isEmpty()))
				throw new IllegalArgumentException("Weka models not defined.");

			if (modelNames.isEmpty())
				throw new IllegalArgumentException("ARFF test data sources not defined.");

			dataSourceFileNames = Validator.validateFilePaths(common.rootDir, dataSourceFileNames, App.ARFF_EXTENSION);
			modelNames = Validator.validateFilePaths(common.rootDir + "/" + modelDir, modelNames, App.MODEL_EXTENSION);

			runMultiThread();

			return 0;
		} catch(Exception e) {
			Utils.logWarn(e.getMessage());

			return 1;
		}
	}
}
