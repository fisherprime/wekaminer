// SPDX-License-Identifier: MIT

package WekaMiner;

import java.util.ArrayList;

public class CompletionCandidates {
	private static final int CANDIDATES_LIMIT = 5;

	static class classifiers extends ArrayList < String > {
		// NOTE: This variable is needed for something; a warning is thrown if
		// it is missing.
		private static final long serialVersionUID = 1L;
		classifiers() {
			super(Finder.getClassifiers(CANDIDATES_LIMIT));
		}
	}

	static class clusterers extends ArrayList < String > {
		private static final long serialVersionUID = 1L;
		clusterers() {
			super(Finder.getClusterers(CANDIDATES_LIMIT));
		}
	}

	static class filters extends ArrayList < String > {
		private static final long serialVersionUID = 1L;
		filters() {
			super(Finder.getFilters(CANDIDATES_LIMIT));
		}
	}

	static class evaluators extends ArrayList < String > {
		private static final long serialVersionUID = 1L;
		evaluators() {
			super(Finder.getEvaluators(CANDIDATES_LIMIT));
		}
	}

	static class searchers extends ArrayList < String > {
		private static final long serialVersionUID = 1L;
		searchers() {
			super(Finder.getSearchers(CANDIDATES_LIMIT));
		}
	}
}
