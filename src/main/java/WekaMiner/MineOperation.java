// SPDX-License-Identifier: MIT

package WekaMiner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import WekaMiner.Cli.CommonOptions;
import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;
import picocli.CommandLine.Parameters;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

@Command(name = "mine",
         versionProvider = ManifestVersionProvider.class,
         usageHelpAutoWidth = true, mixinStandardHelpOptions = true,
         optionListHeading = "%nOptions:%n",
         description = "Run some data mining task on data sets.")
public class MineOperation implements Callable < Integer > {
	@Mixin
	CommonOptions common;

	static class DataPrep {
		@Option(names = {"-s", "--split-data-set"}, split = "/",
		        required = true,
		        arity = "0..3",
		        defaultValue = "70/30/10",
		        fallbackValue = "70/30/10",
		        description = "Slash-separated @|bold (/)|@ list of percentages" +
		                      " (@|bold as integers|@) to split the input data" +
		                      " set(s) into @|fg(yellow) train,test(,eval)|@" +
		                      " data sets.%n" +
		                      "Default: @|fg(green) ${DEFAULT-VALUE}|@.")
		private Integer[] dataSetSplits;

		@Option(names = {"-f", "--filters"}, split = ",", paramLabel = "FILTER",
		        completionCandidates = CompletionCandidates.filters.class,
		        description = "List of filters (attribute &" +
		                      " instance) to run on the data set(s)." +
		                      " The filters will be applied in the @|bold order" +
		                      " specified|@.%n" +
		                      "Values: @|fg(green)" +
		                      " ${COMPLETION-CANDIDATES}, ...|@")
		private ArrayList < String > filters;

		@Option(names = {"-F", "--cascade-filters"},
		        defaultValue = "false",
		        description = "Perform filter operation(s) on the result of" +
		                      " the previous filter operation.%n" +
		                      "Default: @|fg(green) ${DEFAULT-VALUE}|@.")
		private boolean cascadeFilters;

		@Option(names = {"-a", "--select-attributes"}, split = ",",
		        arity = "1..2",
		        description = "Select attributes from the input data set(s).%n" +
		                      "Values: @|fg(green) default|@ or an" +
		                      " evaluator-searcher pair in the form" +
		                      " @|fg(green) evaluator[:option1,option2...]," +
		                      "searcher[:option1,option2...]|@.")
		private ArrayList < String > attributeSelector;
	}

	static class MiningOptions {
		@Option(names = {"-M", "--no-save-model"},
		        defaultValue = "true",
		        description = "Do not save generated models.")
		private boolean saveModel;

		@Option(names = {"-U", "--no-use-unprepared-data"},
		        defaultValue = "true",
		        description = "Do not perform data mining operations on the" +
		                      " unprepared data set (when filters/attribute" +
		                      " selection is specified).")
		private boolean useUnprepared;

		@Option(names = {"-v", "--cross-validate"},
		        defaultValue = "false",
		        description = "Use cross-validation instead of the" +
		                      " train-test approach.")
		private boolean crossValidate;

		@Option(names = {"-c", "--classifiers"}, split = ",",
		        arity = "0..*",
		        paramLabel = "CLASSIFIER",
		        fallbackValue = "default",
		        completionCandidates = CompletionCandidates.classifiers.class,
		        description = "List of comma-separated classification" +
		                      " algorithm(s) to use.%n" +
		                      "Values: @|fg(green) all," +
		                      " default, ${COMPLETION-CANDIDATES}, ...|@")
		private ArrayList < String > classifiers;

		@Option(names = {"-cl", "--clusterers"}, split = ",",
		        arity = "0..*",
		        paramLabel = "CLUSTERER",
		        fallbackValue = "default",
		        completionCandidates = CompletionCandidates.clusterers.class,
		        description = "List of comma-separated clusterer(s) to use.%n" +
		                      "Values : @|fg(green) all, default," +
		                      " ${COMPLETION-CANDIDATES}, ...|@")
		private ArrayList < String > clusterers;

		@Parameters(arity = "1..*", paramLabel = "TRAIN[:TEST]",
		            description = "File(s) containing the train and test" +
		                          " data sets, relative to @|bold" +
		                          " --root-dir|@ in the form: @|fg(green)" +
		                          " train[:test]|@")
		private ArrayList < String > dataSourceFileNames;
	}

	@ArgGroup(validate = false, exclusive = false, heading = "%nData preparation options%n")
	DataPrep dataPrep;

	@ArgGroup(validate = false, exclusive = false, heading = "%nData mining options%n")
	MiningOptions miningOptions;

	private boolean filterDataSet = false;
	private boolean selectAttributes = false;
	private boolean validClassifiers = false;
	private boolean validClusterers = false;
	private boolean validTestDataSources = false;

	public static final String[] DEFAULT_CLASSIFIERS = {"LibSVM", "J48", "IBk"};
	public static final String[] DEFAULT_CLUSTERERS = {"EM", "SimpleKMeans"};
	public static final String[] DEFAULT_EVALUATOR_SEARCHER = {"CfsSubsetEval", "BestFirst"};
	public static final String[] DEFAULT_FILTERS = {"RemoveDuplicates"};

	private ExecutorService executor;

	// FIXME: Reduce this methods arguments.
	private void
	configureMiner(String minerName, String minerOptions, int
	               neighboursClusters, String dataSourceBaseName, Instances[] dataSets,
	               String operationInfo, String preparationsInfo)
	throws IllegalArgumentException {
		try {
			Miner dataMiner = new Miner(minerName, minerOptions, dataSets);

			dataMiner.setPaths(common.rootDir, dataSourceBaseName, common.resultLogPath);

			dataMiner.setOperationInfo(operationInfo);
			dataMiner.setPreparationsInfo(preparationsInfo);

			if (neighboursClusters > 0)
				dataMiner.setNeighboursClusters(neighboursClusters);

			dataMiner.setCrossValidate(miningOptions.crossValidate);
			dataMiner.setSaveModel(miningOptions.saveModel);

			// NOTE: No uncaught exceptions are caught by this operation.

			/* dataMiner.setUncaughtExceptionHandler(
			 *     new Thread.UncaughtExceptionHandler() {
			 *     @Override
			 *     public void uncaughtException(Thread t, Throwable e) {
			 *         Utils.logWarn(e.getMessage());
			 *     }
			 * }); */

			executor.execute(dataMiner);
		} catch(IllegalArgumentException ia) {
			throw ia;
		} catch(Exception e) {
			Utils.logWarn(e.getMessage());
		}
	}

	// FIXME: Sort out this messy method.
	private void
	poolMiningTask(Instances[] dataSets, String dataSourceBaseName, String operationInfo, String preparationsInfo, App.WekaType taskMask) {
		ArrayList < String > minersWithOptions;

		switch (taskMask) {
			case CLASSIFIER:
				Utils.logInfo("Pooling classification tasks.");
				minersWithOptions = miningOptions.classifiers;

				break;
			case CLUSTERER:
				Utils.logInfo("Pooling clustering tasks.");
				minersWithOptions = miningOptions.clusterers;

				break;
			default:
				return;
		}

		String operationInfoBackup = new String(operationInfo);

		for (String minerWithOptions : minersWithOptions) {
			// NOTE: Debug code.
			Utils.logDebug("Miner (+options): " + minerWithOptions);

			String minerName;

			String[] neighboursClustersOptions;

			ArrayList < String > finalOptions;
			ArrayList < String > splitOptions = Utils.splitToolWithOptions(minerWithOptions);

			minerName = splitOptions.get(0);
			splitOptions.remove(0);

			operationInfo = new String(operationInfoBackup + " " + minerName);

			neighboursClustersOptions = Utils.locateNeighboursClustersOption(minerName, splitOptions);

			if (neighboursClustersOptions == null) {
				if (miningOptions.crossValidate)
					operationInfo += " cross-validate";

				configureMiner(minerName,
				               Utils.mergeToolOptions(splitOptions), 0, dataSourceBaseName,
				               dataSets, operationInfo, preparationsInfo);

				continue;
			}

			// NOTE: Using a separate variable `numberSequence` to hold the
			// sequence limit or numberSequence's value gets overwritten.
			Integer[] numberSequence = {};
			Integer[] sequenceLimits = {};

			String iterativeOperationInfoBackup = new String(operationInfo);

			String userNeighboursClustersOption = neighboursClustersOptions[0];
			String minerNeighboursClustersOption = neighboursClustersOptions[1];
			// String neighboursOrClusters = userAndMinerOptions[2];

			sequenceLimits = Utils.getSequenceLimits(userNeighboursClustersOption, minerNeighboursClustersOption);
			numberSequence = sequenceLimits;

			// Remove the user option from the options list.
			splitOptions.remove(userNeighboursClustersOption);

			if (sequenceLimits.length == 2)
				numberSequence = Utils.generateNumberSequence(sequenceLimits);

			// NOTE: Iterate over the number of neighbours/clusters
			// possible for a data miner.
			for (int neighboursClusters : numberSequence) {
				operationInfo = new String(iterativeOperationInfoBackup);
				finalOptions = new ArrayList < String > (splitOptions);

				// NOTE: Options must be separated with a space for weka
				// to properly handle them.
				finalOptions.add(" " + minerNeighboursClustersOption + " " + neighboursClusters);

				operationInfo += String.format("(%d)", neighboursClusters);
				if (miningOptions.crossValidate)
					operationInfo += " cross-validate";

				// NOTE: Debug code.
				Utils.logDebug(
					String.format("User neighbourCluster option: %s \nGenerated number sequence: %s \nTrimmed options: %s \nAltered options: %s",
					              userNeighboursClustersOption, Arrays.toString(numberSequence),
					              Arrays.toString(
									  splitOptions.toArray(new String[0])),
					              Arrays.toString(
									  finalOptions.toArray(new String[0]))));

				configureMiner(minerName,
				               Utils.mergeToolOptions(finalOptions),
				               neighboursClusters, dataSourceBaseName,
				               dataSets, operationInfo, preparationsInfo);
			}
		}
	}

	private void
	splitDataSetAndPoolTask(Instances mainDataSet, String[] testDataSourceArray, String dataSourceBaseName,
	                        DataSetPreparator preparator, StringBuffer preparationsInfo)
	throws Exception {
		if (testDataSourceArray.length < 1) {
			preparator.configureDataSetSplitter(dataPrep.dataSetSplits);
			Instances[] dataSetSplits = preparator.splitDataSet();

			if (validClassifiers)
				poolMiningTask(dataSetSplits, dataSourceBaseName,
				               preparator.getOperationInfo(),
				               preparationsInfo.toString(), App.WekaType.CLASSIFIER);

			if (validClusterers)
				poolMiningTask(dataSetSplits, dataSourceBaseName,
				               preparator.getOperationInfo(),
				               preparationsInfo.toString(), App.WekaType.CLUSTERER);

			return;
		}

		// NOTE: This is an expensive iteration, the `all` value should be
		// avoided.
		// Iterate over a list of test data sources, the main data set is
		// not split.
		for (String testDataSourceFileName: testDataSourceArray) {
			Instances testDataSet = new DataSource(common.rootDir + "/" + testDataSourceFileName).getDataSet();
			ResultBuffer resultBuffer = new ResultBuffer(common.resultLogPath);

			resultBuffer.beginBuffering();
			resultBuffer.addLine("[Test data source summary]: {", 0);
			resultBuffer.addLine("[Source]: " + common.rootDir + "/" + testDataSourceFileName, 1);
			resultBuffer.addLine("[Relation name]: " + testDataSet.relationName(), 1);
			resultBuffer.addLine("[Num instances]: " + testDataSet.numInstances(), 1);
			resultBuffer.addLine("[Num attributes]: " + testDataSet.numAttributes(), 1);
			resultBuffer.addLine("}", 0);
			resultBuffer.endBuffering();

			Instances[] dataSetSplits = {mainDataSet,
				                         testDataSet,
				                         null};

			if (validClassifiers) {
				poolMiningTask(dataSetSplits, dataSourceBaseName,
				               preparator.getOperationInfo(),
				               preparationsInfo.toString(),
				               App.WekaType.CLASSIFIER);
			}

			if (validClusterers) {
				poolMiningTask(dataSetSplits, dataSourceBaseName,
				               preparator.getOperationInfo(),
				               preparationsInfo.toString(),
				               App.WekaType.CLUSTERER);
			}
		}
	}

	private void
	prepareDataSetAndPoolTask(String trainDataSourceFileName, Instances mainDataSet, String[] testDataSourceArray, DataSourceResultLogger resultLogger) throws Exception {
		String dataSourceBaseName = Utils.splitFilenameExt(trainDataSourceFileName).get(0);

		StringBuffer operationInfo;
		StringBuffer operationInfoBackup;
		StringBuffer preparationsInfo;
		StringBuffer preparationsInfoBackup;

		Instances mainDataSetBackup = null;

		Utils.logDataSetSummary(common.resultLogPath, mainDataSet.toSummaryString());

		if (mainDataSet.classIndex() == -1)
			mainDataSet.setClassIndex(mainDataSet.numAttributes() - 1);

		operationInfo = new StringBuffer(trainDataSourceFileName);
		operationInfoBackup = new StringBuffer(operationInfo);

		preparationsInfo = new StringBuffer();
		preparationsInfoBackup = new StringBuffer(preparationsInfo);

		if (miningOptions.useUnprepared) {
			DataSetPreparator preparator = new DataSetPreparator(mainDataSet,
			                                                     common.resultLogPath, operationInfo, preparationsInfo);
			splitDataSetAndPoolTask(mainDataSet, testDataSourceArray, dataSourceBaseName, preparator, preparationsInfo);
		}

		// NOTE: no filter specified.
		if (!filterDataSet) {
			if (!selectAttributes)
				// NOTE: No filter was specified, operations are complete.
				return;

			// Work on a instance of the input data set(s) that has specific
			// attributes --as decided by the attribute selector--.

			DataSetPreparator preparator = new DataSetPreparator(mainDataSet,
			                                                     common.resultLogPath, operationInfo, preparationsInfo);

			preparator.configureAttributeSelector(dataPrep.attributeSelector.toArray(new String[0]));
			preparator.selectDataSetAttributes();

			splitDataSetAndPoolTask(mainDataSet, testDataSourceArray, dataSourceBaseName, preparator, preparationsInfo);

			return;
		}

		operationInfo = new StringBuffer(operationInfoBackup);
		preparationsInfoBackup = new StringBuffer(preparationsInfo);

		if (!dataPrep.cascadeFilters)
			mainDataSetBackup = new Instances(mainDataSet);

		for (String filterWithOptions : dataPrep.filters) {
			if (!dataPrep.cascadeFilters) {
				operationInfo = new StringBuffer(operationInfoBackup);
				preparationsInfo = new StringBuffer(preparationsInfoBackup);
				mainDataSet = new Instances(mainDataSetBackup);
			}

			DataSetPreparator preparator = new DataSetPreparator(mainDataSet,
			                                                     common.resultLogPath, operationInfo, preparationsInfo);

			try {
				preparator.configureFilter(filterWithOptions);
				preparator.filterDataSet();
			} catch(Exception e) {
				Utils.logWarn(e.getMessage());

				continue;
			}

			if (selectAttributes) {
				try {
					preparator.configureAttributeSelector(dataPrep.attributeSelector.toArray(new String[0]));
					preparator.selectDataSetAttributes();
				} catch(Exception e) {
					Utils.logWarn(e.getMessage());

					continue;
				}
			}

			splitDataSetAndPoolTask(mainDataSet, testDataSourceArray, dataSourceBaseName, preparator, preparationsInfo);
		}
	}

	// NOTE: Separating the log files will allow for no blocking; may
	// implement if needed.
	private void
	runMultiThread() throws Exception {
		Utils.updateResultLog(common.resultLogPath, "CLI arguments: " + Arrays.toString(App.cliArguments) + "\n\n");

		boolean firstDataSource = true;

		for (String dataSources : miningOptions.dataSourceFileNames) {
			String[] dataSourceArray = dataSources.split(":");
			String[] trainDataSourceArray = dataSourceArray[0].split(",");
			String[] testDataSourceArray = {};

			// The array contains train & test data source file names.
			if (dataSourceArray.length == 2)
				testDataSourceArray = dataSourceArray[1].split(",");

			// NOTE: Loop for the use case: `all:all all:sth.arff`.
			for (String trainDataSourceFileName: trainDataSourceArray) {
				String trainDataSource = common.rootDir + "/" + trainDataSourceFileName;

				Instances mainDataSet = null;

				DataSourceResultLogger resultLogger = new DataSourceResultLogger(common.resultLogPath, trainDataSource, firstDataSource);
				resultLogger.beginLogging();

				try {
					// Create an asynchronous task manager for each data
					// source.
					executor = Executors.newWorkStealingPool();

					// NOTE: obtaining the data set at this position to reduce
					// memory usage. A reference to this data set is passed to
					// methods instead of deep copies.
					mainDataSet = new DataSource(trainDataSource).getDataSet();

					prepareDataSetAndPoolTask(trainDataSourceFileName, mainDataSet, testDataSourceArray, resultLogger);
					Utils.shutdownAndAwaitTermination(executor);
				} catch(Exception e) {
					if (executor != null)
						executor.shutdownNow();

					Utils.updateResultLog(common.resultLogPath,
					                      "\nMining failed for the data source at: " + trainDataSource + "\n\n");
					Utils.logWarn(e.getMessage());
				}

				// NOTE: The garbage collector isn't fast enough in certain
				// cases, the shared data set should be deleted at this point.
				if (mainDataSet != null)
					mainDataSet = null;

				resultLogger.endLogging();
				firstDataSource = false;
			}
		}
	}

	/**
	 * Validate a list of train(:test) data source pairs.
	 *
	 * No guards against expensive choices: `all:all all:sth.arff`.
	 *
	 * TODO: Look into grouping train & test data sources.
	 */
	private ArrayList < String >
	validateDataSources() throws Exception {
		LinkedHashSet < String > validDataSourceSet = new LinkedHashSet < String > ();

		for (String dataSourceFiles: miningOptions.dataSourceFileNames) {
			boolean firstDataSource = true;

			String mergedSourceStr;

			String[] dataSourceArray = dataSourceFiles.split(":");

			ArrayList < String > trainDataSources = new ArrayList < String > ();
			ArrayList < String > testDataSources = new ArrayList < String > ();

			for (String dataSourceFileName: dataSourceArray) {
				try {
					if (firstDataSource) {
						trainDataSources = Validator.validateFilePath(common.rootDir, dataSourceFileName, App.ARFF_EXTENSION);
					} else {
						testDataSources = Validator.validateFilePath(common.rootDir, dataSourceFileName, App.ARFF_EXTENSION);
						validTestDataSources = true;
					}
					firstDataSource = false;
				} catch(IllegalArgumentException ia) {
					if (firstDataSource)
						throw new IllegalArgumentException("Train data source: " + ia.getMessage());
					throw new IllegalArgumentException("Test data source: " + ia.getMessage());
				}
			}

			mergedSourceStr = Utils.mergeSplitList(trainDataSources, ",");
			if (!testDataSources.isEmpty())
				mergedSourceStr += ":" + Utils.mergeSplitList(testDataSources, ",");

			validDataSourceSet.add(mergedSourceStr);

			// Debug code.
			Utils.logDebug(
				String.format("Train(:test) data sources: %s \nValidSources list: %s",
				              mergedSourceStr, Arrays.toString(validDataSourceSet.toArray(new String[0])))
				);
		}

		return new ArrayList < String > (validDataSourceSet);
	}

	private void
	validateMiningOptions() throws Exception {
		if (miningOptions == null)
			throw new Exception("No data mining operation has been specified.");

		if ((miningOptions.dataSourceFileNames == null) || (miningOptions.dataSourceFileNames.isEmpty()))
			throw new Exception("No data source has been specified.");

		miningOptions.dataSourceFileNames = validateDataSources();

		if ((miningOptions.classifiers != null) && !miningOptions.classifiers.isEmpty()) {
			miningOptions.classifiers = Validator.validateTools(miningOptions.classifiers, App.WekaType.CLASSIFIER);
			validClassifiers = true;
		}

		if ((miningOptions.clusterers != null) && !miningOptions.clusterers.isEmpty()) {
			miningOptions.clusterers = Validator.validateTools(miningOptions.clusterers, App.WekaType.CLUSTERER);
			validClusterers = true;
		}
	}

	private void
	validateDataPrepOptions() throws Exception {
		if (dataPrep == null)
			return;

		if (validTestDataSources)
			Utils.logInfo("Data set split percentages will be ignored where test data sets are specified.");
		Validator.validateDataSetSplits(dataPrep.dataSetSplits);

		if ((dataPrep.attributeSelector != null) && !dataPrep.attributeSelector.isEmpty()) {
			if (dataPrep.attributeSelector.get(0).equals("default")) {
				Utils.logInfo("Defaulting attribute selector to: " +
				              Arrays.toString(DEFAULT_EVALUATOR_SEARCHER));

				dataPrep.attributeSelector = new ArrayList < String > (Arrays.asList(DEFAULT_EVALUATOR_SEARCHER));
			}

			if (dataPrep.attributeSelector.size() != 2)
				throw new IllegalArgumentException(
					"The attribute selector option takes an evaluator-searcher pair of the form: EVALUATOR,SEARCHER");

			dataPrep.attributeSelector = Validator.validateToolWithOptions(dataPrep.attributeSelector, App.WekaType.ATTRIBUTE_SELECTOR);

			// NOTE: Debug code.
			Utils.logDebug("Atribute selector --evaluator, searcher-- (+options): " + Arrays.toString(
							   dataPrep.attributeSelector.toArray(new String[0])));

			if (!dataPrep.attributeSelector.isEmpty())
				selectAttributes = true;
		}

		if ((dataPrep.filters != null) && !dataPrep.filters.isEmpty()) {
			dataPrep.filters = Validator.validateToolWithOptions(dataPrep.filters, App.WekaType.FILTER);

			// NOTE: Debug code.
			Utils.logDebug("Filters (+options): " + Arrays.toString(
							   dataPrep.filters.toArray(new String[0])));

			if (!dataPrep.filters.isEmpty())
				filterDataSet = true;
		}
	}

	@Override
	public Integer
	call() {
		try {
			common.validateRootDir();
			common.validateResultLogPath();

			Utils.logInfo("Running WekaMiner");

			validateMiningOptions();
			validateDataPrepOptions();

			if (!miningOptions.useUnprepared && !selectAttributes && !filterDataSet)
				throw new Exception("No operation will be performed," +
				                    " consider using the unprepared data set" +
				                    " or specify filters for the data set.");

			runMultiThread();

			return 0;
		} catch(Exception e) {
			Utils.logWarn(e.getMessage());

			return 1;
		}
	}
}
