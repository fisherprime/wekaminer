// SPDX-License-Identifier: MIT

package WekaMiner;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import WekaMiner.Cli.CommonOptions;
import picocli.CommandLine.Command;
import picocli.CommandLine.Mixin;
import picocli.CommandLine.Option;

// TODO: Look into handling all the filetypes Weka supports.
// Select converter based on the filetype extension.
@Command(name = "convert",
         versionProvider = ManifestVersionProvider.class,
         usageHelpAutoWidth = true, mixinStandardHelpOptions = true,
         optionListHeading = "%nOptions:%n",
         description = "Convert a @|fg(yellow) CSV|@ data set to an @|fg(yellow) ARFF|@ data set.")
public class ConvertOperation implements Callable < Integer > {
	@Mixin
	CommonOptions common;

	@Option(names = {"-I", "--csv-input"}, paramLabel = "FILE",
	        description = "CSV file to convert to ARFF.")
	private String csvFile;

	@Option(names = {"-O", "--arff-output"}, paramLabel = "FILE",
	        description = "Filename to output ARFF conversion to.")
	private String arffFile;

	private void
	convertDataSource() {
		String inputFile = common.rootDir + "/" + csvFile;

		ArrayList < String > outputFileSplit = Utils.splitFilenameExt(arffFile);
		if (outputFileSplit.size() == 1)
			outputFileSplit.add(App.ARFF_EXTENSION);

		String outputFile = Validator.validateSaveFileName(
			common.rootDir + "/" + outputFileSplit.get(0), outputFileSplit.get(1));

		Utils.convertCsvToArff(inputFile, outputFile);
	}

	@Override
	public Integer
	call() {
		try {
			common.validateRootDir();

			if ((csvFile.isBlank()))
				throw new IllegalArgumentException("CSV input file not defined.");

			if (arffFile.isBlank())
				throw new IllegalArgumentException("ARFF output file not defined.");

			convertDataSource();

			return 0;
		} catch(Exception e) {
			Utils.logWarn(e.getMessage());

			return 1;
		}
	}
}
