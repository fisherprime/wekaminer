// SPDX-License-Identifier: MIT

package WekaMiner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Prediction;
import weka.clusterers.Clusterer;
import weka.core.Instances;
import weka.core.OptionHandler;
import weka.core.converters.ArffSaver;
import weka.core.converters.CSVLoader;

public class Utils {
	public static final String CLUSTERS_OPTION_MATCH = "umber of cluster";
	public static final String NEIGHBOURS_OPTION_MATCH = "umber of nearest neighbour";

	public static final String CLUSTERS_ID = "clusters";
	public static final String NEIGHBOURS_ID = "neighbours";

	public static final Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

	public static void
	logInfo(String message) {
		System.out.println("INFO: " + message);
	}

	public static void
	logWarn(String message) {
		LOGGER.warning(message);
	}

	public static void
	logDebug(String message) {
		if (!App.DEBUG_MODE)
			return;

		System.out.println(
			String.format("\n%s \nDEBUG: %s\n",  LocalDateTime.now(),
			              message));
	}

	public static ArrayList < String >
	splitString(String options, String delimiter) {
		return new ArrayList < String > (Arrays.asList(options.split(delimiter)));
	}

	// Number generation code provided by Prince.
	public static Integer[]
	generateOddNumbers(Integer[] range) {
		Set < Integer > oddNumbers = new HashSet < > ();

		for (int i = range[0]; i <= range[1]; ++i)
			if ((double) i % 2 != 0) oddNumbers.add(i);

		return oddNumbers.toArray(new Integer[oddNumbers.size()]);
	}

	public static Integer[]
	generateEvenNumbers(Integer[] range) {
		Set < Integer > evenNumbers = new HashSet < > ();

		for (int i = range[0]; i <= range[1]; ++i)
			if ((double) i % 2 == 0) evenNumbers.add(i);

		return evenNumbers.toArray(new Integer[evenNumbers.size()]);
	}

	public static Integer[]
	generateNumberSequence(Integer[] range) {
		Set < Integer > numbers = new HashSet < > ();

		for (int i = range[0]; i <= range[1]; ++i)
			numbers.add(i);

		return numbers.toArray(new Integer[numbers.size()]);
	}

	public static String
	stringRepeat(String str, int frequency) {
		return String.join("", Collections.nCopies(frequency, str));
	}

	/**
	 * Returns the current time converted to a user specified time unit.
	 *
	 * THe time unit defaults to milliseconds.
	 */
	public static long
	now(TimeUnit... targetUnit) {
		if ((targetUnit != null) && (targetUnit.length > 0) &&
		    !targetUnit[0].equals(TimeUnit.MILLISECONDS))
			return targetUnit[0].convert(Duration.ofMillis(System.currentTimeMillis()));

		return System.currentTimeMillis();
	}

	/**
	 * Calculates the difference between `now` & a user specified time (in
	 * milliseconds) converted to some time unit.
	 *
	 * The time conversion unit defaults to milliseconds.
	 */
	public static long
	getDifferenceFromNow(long time, TimeUnit... targetUnit) {
		// TODO: Sort out truncation that occurs from the conversion to coarse
		// units.
		if ((targetUnit != null) && (targetUnit.length > 0) &&
		    !targetUnit[0].equals(TimeUnit.MILLISECONDS))
			return targetUnit[0].convert(Duration.ofMillis(now() - time));

		return now() - time;
	}

	public static Void
	createDirectory(String path) throws IllegalArgumentException {
		if (path.isBlank())
			throw new IllegalArgumentException("The path provided is blank.");

		File dir = new File(path);

		if (!dir.exists()) {
			dir.mkdirs();
			Utils.logInfo("Directory hierarchy created: " + path);
		}

		return null;
	}

	public static ArrayList < String >
	splitFilenameExt(String fileName) {
		return splitString(fileName, "\\.(?=[^\\.]+$)");
	}

	public static String[]
	locateNeighboursClustersOption(String minerName, ArrayList < String > splitUserOptions)
	throws IllegalArgumentException {
		// NOTE:
		// The process flow is as follows:
		// Obtain an enumeration of the classifier/clusterer's options;
		// Iterate over the enumeration looking for a "number of
		// clusters"/"number of nearest neighbours" option; and
		// Return the user specified option containing a
		// ...clusterers/...nearest neighbours option & an identifier for
		// either of the desired options.

		for (String match: Finder.getWekaMatches(minerName)) {
			try {
				// Reflect class for classifier/clusterer/filter.
				Class < ? > toolClass = Class.forName(match);
				Object classInstance = toolClass.getDeclaredConstructor().newInstance();

				if (!(classInstance instanceof Classifier) && !Clusterer.class.isAssignableFrom(toolClass))
					throw new IllegalArgumentException(String.format("%s is neither a classifier nor a clusterer.", minerName));

				// @SuppressWarnings("unchecked")
				Enumeration < weka.core.Option > toolOptions = ((OptionHandler) classInstance).listOptions();

				while (toolOptions.hasMoreElements()) {
					weka.core.Option toolOption = toolOptions.nextElement();

					boolean useClusters = toolOption.description().contains(CLUSTERS_OPTION_MATCH);
					boolean useNeighbours = toolOption.description().contains(NEIGHBOURS_OPTION_MATCH);

					if (useClusters || useNeighbours) {
						String neighboursClustersOption = toolOption.name();

						// NOTE: The clusters/neighbours identifier is not
						// used.
						for (String userOption: splitUserOptions)
							if (userOption.contains(neighboursClustersOption))
								return new String[] {userOption, "-" + toolOption.name(), useClusters == true ? CLUSTERS_ID : NEIGHBOURS_ID};
					}
				}
			} catch(Exception e) {
				// Do nothing.
			}
		}

		return null;
	}


	public static Integer []
	getSequenceLimits(String sequenceOption, String toolNeighboursClustersOption) throws IllegalArgumentException {
		ArrayList < Integer > sequenceLimits = new ArrayList < Integer > ();

		// NOTE: `split` seems cleaner than `replace`.
		String[] numberSequenceSplit = sequenceOption.split(toolNeighboursClustersOption);

		// NOTE: Using a loop to handle white spaces & empty strings.
		for (int iter = 0; iter < numberSequenceSplit.length; ++iter) {
			String splitItem = numberSequenceSplit[iter].strip();

			if (splitItem.isBlank())
				continue;

			if (splitItem.matches("[0-9]+-[0-9]+")) {
				for (String number: splitItem.split("-")) {
					try {
						sequenceLimits.add(Integer.parseInt(number));
					} catch(NumberFormatException n) {
						throw new IllegalArgumentException(
							"Error parsing neighbour/cluster sequence range: " + n);
					}
				}

				break;
			} else if (splitItem.matches("[0-9]+")) {
				return new Integer[] {Integer.parseInt(splitItem)};
			}
		}

		if (sequenceLimits.size() != 2)
			throw new IllegalArgumentException("The neighbour/cluster sequence range takes 2 values");

		if (sequenceLimits.get(0) > sequenceLimits.get(1))
			throw new IllegalArgumentException(
				"The upper range limit is smaller than the lower range limit: " +
				Arrays.toString(sequenceLimits.toArray(new Integer[0])));

		return sequenceLimits.toArray(new Integer[0]);
	}

	public static ArrayList < String >
	splitToolWithOptions(String options) {
		return splitString(options, ":");
	}

	public static String
	mergeSplitList(ArrayList < String > splitList, String... delimiter) {
		boolean firstEntry = true;

		StringBuffer finalList = new StringBuffer();

		if ((delimiter != null) && (delimiter.length == 1)) {
			for (String argument : splitList) {
				if (!firstEntry)
					finalList.append(delimiter[0]);
				finalList.append(argument);
			}

			return finalList.toString();
		}

		for (String argument: splitList)
			finalList.append(argument);

		return finalList.toString();
	}

	/**
	 * Combines a split tool option array.
	 */
	public static String
	mergeToolOptions(ArrayList < String > splitOptions) {
		return mergeSplitList(splitOptions);
	}

	public static void
	updateResultLog(String resultLogPath, String content) {
		try {
			FileWriter writer = new FileWriter(resultLogPath, true);

			writer.write(content);

			writer.flush();
			writer.close();
		} catch(IOException io) {
			Utils.logWarn(io.getMessage());
		}
	}

	public static void
	writePredictionsToCsv(List < Prediction > predictions, String outFile) {
		try {
			if (predictions.isEmpty())
				return;

			int iter = 0;

			// Populate header.
			StringBuilder csvOutput = new StringBuilder("#instance,actual,predicted,weight\n");

			FileWriter writer = new FileWriter(outFile);

			for (Prediction prediction : predictions) {
				// Write data.
				csvOutput.append(
					String.format("%d,%f,%f,%f\n", iter, prediction.actual(),
					              prediction.predicted(), prediction.weight()));
				++iter;
			}

			writer.write(csvOutput.toString());

			// Terminate
			writer.flush();
			writer.close();
		} catch(IOException io) {
			Utils.logWarn(io.getMessage());
		}
	}

	public static void
	convertCsvToArff(String filePath, String OutputPath) {
		try {
			CSVLoader loader = new CSVLoader();
			File csvFile = new File(filePath);

			if (!csvFile.exists()) {
				Utils.logWarn(String.format("CSV file: %s does not exist.", filePath));

				return;
			}

			loader.setSource(new File(filePath));
			Instances data = loader.getDataSet();

			ArffSaver saver = new ArffSaver();
			saver.setInstances(data);
			saver.setFile(new File(OutputPath));

			saver.writeBatch();
		} catch(IOException io) {
			Utils.logWarn(io.getMessage());
		}
	}

	// REF: Java `ExecutorService` docs.
	public static void
	shutdownAndAwaitTermination(ExecutorService executor) {
		// Initiate an orderly shutdown in which previously submitted
		// tasks
		// are executed, but no new tasks will be accepted.
		executor.shutdown();

		try {
			// Block execution until all tasks for the current file
			// complete.
			if (!executor.awaitTermination(Long.MAX_VALUE, TimeUnit.SECONDS)) {
				executor.shutdownNow();

				if (!executor.awaitTermination(60, TimeUnit.SECONDS))
					Utils.logWarn("WekaMiner tasks pool did not terminate.");
			}
		} catch(InterruptedException ie) {
			// (Re-)Cancel if current thread also interrupted.
			executor.shutdownNow();

			// Preserve interrupt status.
			Thread.currentThread().interrupt();
		}
	}

	public static void
	logDataSetSummary(String resultLogPath, String summary) {
		updateResultLog(resultLogPath, "\n[Data source summary]: {\n");

		for (String line : summary.split("\n"))
			updateResultLog(resultLogPath, (line.isBlank() ? "" : "\t") + line + "\n");

		updateResultLog(resultLogPath, "}\n");
	}
}
