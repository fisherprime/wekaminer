// SPDX-License-Identifier: MIT

package WekaMiner;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

import picocli.CommandLine;
import picocli.CommandLine.IVersionProvider;

// REF:
// https://github.com/remkop/picocli/blob/master/picocli-examples/src/main/java/picocli/examples/VersionProviderDemo2.java
public class ManifestVersionProvider implements IVersionProvider {
	public String[]
	getVersion() throws Exception {
		Enumeration < URL > resources = CommandLine.class.getClassLoader().getResources("META-INF/MANIFEST.MF");

		while (resources.hasMoreElements()) {
			URL url = resources.nextElement();

			try {
				Manifest manifest = new Manifest(url.openStream());
				if (isApplicableManifest(manifest)) {
					Attributes attr = manifest.getMainAttributes();

					return new String[] {"${COMMAND-FULL-NAME}" + " v" +
							             get(attr, "Implementation-Version"),
							             "Built: " + get(attr, "Buildtime")};
				}
			} catch(IOException io) {
				return new String[] {"Unable to read from " + url + ": " + io};
			}
		}

		return new String[0];
	}

	private boolean
	isApplicableManifest(Manifest manifest) {
		Attributes attributes = manifest.getMainAttributes();

		return "WekaMiner".equals(get(attributes, "Implementation-Title"));
	}

	private static Object
	get(Attributes attributes, String key) {
		return attributes.get(new Attributes.Name(key));
	}
}
