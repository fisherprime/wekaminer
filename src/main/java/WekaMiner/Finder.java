// SPDX-License-Identifier: MIT

package WekaMiner;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import org.reflections.Reflections;
import org.reflections.ReflectionsException;
import org.reflections.serializers.JsonSerializer;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.FilterBuilder;
import org.reflections.vfs.Vfs;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.classifiers.AbstractClassifier;
import weka.clusterers.AbstractClusterer;
import weka.filters.Filter;

class Finder {
	// ???: Is this a package prefix/resource prefix, ... The docs, examples
	// & implementation are conflicting.
	private static final String PACKAGE_PREFIX = "META-INF/reflections";

	private static final String REFLECTIONS_FILE = "WekaMiner-reflections.json";

	/**
	 * Find classes implemented in Wekaby their class name.
	 *
	 *	NOTE: This search will identify `runnable` classes as defined in:
	 *	`Run.findSchemeMatch(Class<?> classType, String schemeToFind, boolean
	 *	matchAnywhere, boolean notJustRunnables)`.
	 *
	 *	FIXME: Calls to `weka.Run.findSchemeMatch` throw a
	 *	`java.util.zip.ZipException`; haven't identified a way to catch them.
	 */
	public static List < String > getWekaMatches(String toolClass) throws
	IllegalArgumentException {
		List < String > matches = weka.Run.findSchemeMatch(toolClass, true);

		if (matches.size() > 0)
			return matches;

		// Search for attribute selection `searchers` in non-`runnable`s.
		matches = weka.Run.findSchemeMatch(ASSearch.class, toolClass, false, true);
		if (matches.size() > 0)
			return matches;

		throw new IllegalArgumentException(
			String.format("%s is not implemented.", toolClass));
	}

	// NOTE: This method is copied from `Reflections.collect()`.
	@SuppressWarnings("unused")
	private static Reflections
	hackedGetReflections() {
		Collection < URL > urls =
			ClasspathHelper.forPackage(PACKAGE_PREFIX);
		if (urls.isEmpty()) return null;

		Reflections reflections = new Reflections();
		JsonSerializer serializer = new JsonSerializer();

		Iterable < Vfs.File > files = Vfs.findFiles(urls, PACKAGE_PREFIX,
		                                            new FilterBuilder().include(REFLECTIONS_FILE));
		for (final Vfs.File file : files) {
			InputStream inputStream = null;
			try {
				// NOTE: Debug code.
				Utils.logDebug(file.getName());

				inputStream = file.openInputStream();
				reflections.merge(serializer.read(inputStream));
			} catch(IOException e) {
				throw new ReflectionsException("could not merge " + file, e);
			} finally {
				try {
					inputStream.close();
				} catch(IOException io) {
					Utils.logWarn(io.getMessage());
				}
			}
		}

		return reflections;
	}

	private static Reflections
	getReflections() {
		return Reflections.collect(PACKAGE_PREFIX,
		                           new FilterBuilder().include(REFLECTIONS_FILE),
		                           new JsonSerializer());
	}

	public static ArrayList < String >
	getTools(App.WekaType typeMask, int... limit) {
		int toolLimit = 0;

		if ((limit != null) && (limit.length == 1))
			toolLimit = limit[0];

		switch (typeMask) {
			case CLASSIFIER:
				return getClassifiers(toolLimit);
			case CLUSTERER:
				return getClusterers(toolLimit);
			case FILTER:
				// Not a wise choice.
				return new ArrayList < String > ();
			case ATTRIBUTE_SELECTOR:
				// Not a wise choice.
				return new ArrayList < String > ();
			default:
				// Should not occur.
				return new ArrayList < String > ();
		}
	}

	public static ArrayList < String >
	getClassifiers(int limit) {
		ArrayList < String > list = new ArrayList < String > ();

		Reflections reflections = getReflections();

		Set < Class < ? extends AbstractClassifier >> classifiers =
			reflections.getSubTypesOf(weka.classifiers.AbstractClassifier.class);

		for (Class < ? > classifier : classifiers) {
			String classifierName = classifier.getName();
			// Filter out supertype classes.
			if ((reflections.getSubTypesOf(classifier).isEmpty())) {
				try {
					// Filter out non-`runnable` `AbstractClassifiers`
					// classes.
					getWekaMatches(classifierName);
					list.add(classifier.getName());

					if (list.size() == limit)
						return list;
				} catch(IllegalArgumentException ia) {
					// Do nothing.
				}
			}
		}

		return list;
	}

	public static ArrayList < String >
	getClusterers(int limit) {
		ArrayList < String > list = new ArrayList < String > ();

		Reflections reflections = getReflections();

		Set < Class < ? extends AbstractClusterer >> clusterers =
			reflections.getSubTypesOf(weka.clusterers.AbstractClusterer.class);

		for (Class < ? > clusterer : clusterers)
			if (reflections.getSubTypesOf(clusterer).isEmpty()) {
				list.add(clusterer.getName());
				if (list.size() == limit)
					return list;
			}

		return list;
	}

	public static ArrayList < String >
	getFilters(int limit) {
		ArrayList < String > list = new ArrayList < String > ();

		Reflections reflections = getReflections();

		Set < Class < ? extends Filter >> filters =
			reflections.getSubTypesOf(weka.filters.Filter.class);

		for (Class < ? > filter : filters)
			if (reflections.getSubTypesOf(filter).isEmpty()) {
				list.add(filter.getName());
				if (list.size() == limit)
					return list;
			}

		return list;
	}

	public static ArrayList < String >
	getEvaluators(int limit) {
		ArrayList < String > list = new ArrayList < String > ();

		Reflections reflections = getReflections();

		Set < Class < ? extends ASEvaluation >> evaluators =
			reflections.getSubTypesOf(weka.attributeSelection.ASEvaluation.class);

		for (Class < ? > evaluator : evaluators)
			if (reflections.getSubTypesOf(evaluator).isEmpty()) {
				list.add(evaluator.getName());
				if (list.size() == limit)
					return list;
			}

		return list;
	}

	public static ArrayList < String >
	getSearchers(int limit) {
		ArrayList < String > list = new ArrayList < String > ();

		Reflections reflections = getReflections();

		Set < Class < ? extends ASSearch >> searchers =
			reflections.getSubTypesOf(weka.attributeSelection.ASSearch.class);

		for (Class < ? > searcher : searchers)
			if (reflections.getSubTypesOf(searcher).isEmpty()) {
				list.add(searcher.getName());
				if (list.size() == limit)
					return list;
			}

		return list;
	}
}
