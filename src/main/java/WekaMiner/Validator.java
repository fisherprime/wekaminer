// SPDX-License-Identifier: MIT

package WekaMiner;

import java.io.File;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;

import weka.attributeSelection.ASEvaluation;
import weka.attributeSelection.ASSearch;
import weka.classifiers.AbstractClassifier;
import weka.clusterers.AbstractClusterer;
import weka.filters.Filter;

public class Validator {
	public static ArrayList < String >
	validateToolWithOptions(ArrayList < String > toolChoices, App.WekaType typeMask) throws IllegalArgumentException {
		// NOTE: The use of a `Set` subclass is necessary, using `LibSVM`
		// resulted in 3 instances on the `ArrayList`.
		LinkedHashSet < String > validList = new LinkedHashSet < String > ();

		String typeName = "";

		switch (typeMask) {
			case CLASSIFIER:
				typeName = "classifier";
				break;
			case CLUSTERER:
				typeName = "clusterer";
				break;
			case ATTRIBUTE_SELECTOR:
				typeName = "attribute selector";
				break;
			case FILTER:
				typeName = "filter";
				break;
			default:
				typeName = "--unknown type--";
		}

		for (String tool: toolChoices) {
			try {
				for (String match:
				     Finder.getWekaMatches(Utils.splitToolWithOptions(tool).get(0))) {
					Class < ? > toolClass = Class.forName(match);

					switch (typeMask) {
						case CLASSIFIER:
							if ((AbstractClassifier.class.isAssignableFrom(toolClass)) ||
							    tool.equals("LibSVM")) {
								validList.add(tool);
								continue;
							}
							break;
						case CLUSTERER:
							if (AbstractClusterer.class.isAssignableFrom(toolClass)) {
								validList.add(tool);
								continue;
							}
							break;
						case FILTER:
							if (Filter.class.isAssignableFrom(toolClass)) {
								validList.add(tool);
								continue;
							}
							break;
						case ATTRIBUTE_SELECTOR:
							if ((ASSearch.class.isAssignableFrom(toolClass)) ||
							    ASEvaluation.class.isAssignableFrom(toolClass)) {
								validList.add(tool);

								continue;
							}

							// The first value in the evaluator-searcher pair
							// is the evaluator.
							if (toolChoices.get(0).equals(tool))
								Utils.logWarn(String.format("Invalid attribute selector evaluator: %s.", tool));
							else
								Utils.logWarn(String.format("Invalid attribute selector searcher: %s.", tool));

							continue;
						default:
					}

					Utils.logWarn(String.format("Invalid %s: %s.", typeName, tool));
				}
			} catch(Exception e) {
				Utils.logWarn(e.getMessage());
			}
		}

		if (validList.isEmpty())
			throw new IllegalArgumentException(
				String.format("None of the specified %s(s) are implemented: %s.",
				              typeName, Arrays.toString(toolChoices.toArray())));

		return new ArrayList < String > (validList);
	}

	public static ArrayList < String >
	validateTools(ArrayList < String > tools, App.WekaType typeMask) throws IllegalArgumentException {
		String toolType = "";
		String capitalizedToolType = "";

		String[] defaultToolSet = {};

		ArrayList < String > validList = new ArrayList < String > ();

		switch (typeMask) {
			case CLASSIFIER:
				toolType = "classifiers";
				capitalizedToolType = "Classifiers";
				defaultToolSet = MineOperation.DEFAULT_CLASSIFIERS;

				break;
			case CLUSTERER:
				toolType = "clusterers";
				capitalizedToolType = "Clusterers";
				defaultToolSet = MineOperation.DEFAULT_CLUSTERERS;

				break;
			case FILTER:
				if (tools.get(0).equals("all"))
					throw new IllegalArgumentException(
						"`all` is not a valid option for filters.");

				toolType = "filters";
				capitalizedToolType = "Filters";
				defaultToolSet = MineOperation.DEFAULT_FILTERS;

				break;
			case ATTRIBUTE_SELECTOR:
				if (tools.get(0).equals("all"))
					throw new IllegalArgumentException(
						"`all` is not a valid option for attribute selectors.");

				toolType = "attribute selectors";
				capitalizedToolType = "Attribute selectors";
				defaultToolSet = MineOperation.DEFAULT_EVALUATOR_SEARCHER;

				break;
			default:
				// Should not occur.
		}

		switch (tools.get(0)) {
			case "all":
				return Finder.getTools(typeMask);
			case "default":
				if (tools.size() == 1) {
					Utils.logInfo(
						String.format("Defaulting %s to: %s", toolType,
						              Arrays.toString(defaultToolSet)));
					validList = new ArrayList < String > (Arrays.asList(defaultToolSet));

					break;
				}

				tools.remove(0);
				validList.addAll(tools);
			// Fallthrough if additional tools are specified.
			default:
				validList.addAll(Validator.validateToolWithOptions(tools, typeMask));
		}

		// NOTE: Debug code.
		Utils.logDebug(
			String.format("%s (+options): %s", capitalizedToolType,
			              Arrays.toString(validList.toArray(new String[0]))));

		return validList;
	}

	public static boolean
	validatePathExists(String path) throws IllegalArgumentException {
		if (path.isBlank())
			throw new IllegalArgumentException("The path specified is blank.");

		return new File(path).exists();
	}

	public static String
	validateSaveFileName(String filePath, String extension) {
		int counter = 0;

		String finalFilePath = filePath + "." + extension;

		File file = new File(finalFilePath);

		while (file.exists()) {
			finalFilePath = filePath + "(" + (++counter) + ")." + extension;
			file = new File(finalFilePath);
		}

		return finalFilePath;
	}

	/**
	 * Validate a single file path.
	 */
	public static ArrayList < String >
	validateFilePath(String rootDir, String filePath, String extension) throws Exception {
		return validateFilePaths(rootDir,
		                         new ArrayList < String > (Arrays.asList(filePath)),
		                         extension);
	}

	/**
	 * Validate an `ArrayList` (multiple) of file paths.
	 *
	 */
	public static ArrayList < String >
	validateFilePaths(String rootDir, ArrayList < String > filePaths, String extension) throws Exception {
		LinkedHashSet < String > validPathsSet = new LinkedHashSet < > ();

		Utils.logDebug(String.format("Path: %s \nExtension: %s", rootDir, extension));

		// TODO: Validate this operation with edge cases.
		switch (filePaths.get(0)) {
			case "all":
				try(DirectoryStream < Path > dirStream = Files.newDirectoryStream(Paths.get(rootDir))) {
					for (Path entry : dirStream) {
						String entryFilename = entry.getFileName().toString();
						ArrayList < String > filenameSplit = new ArrayList < String > (Utils.splitFilenameExt(entryFilename));

						// NOTE: Debug code.
						Utils.logDebug("Identified file for (all): " + Arrays.toString(filenameSplit.toArray()));


						// NOTE: Using `==` will fail,
						// `contentEquals`/`equals` is required.
						if ((filenameSplit.size() == 2) && (filenameSplit.get(1).equals(extension)))
							validPathsSet.add(entryFilename);
					}
				}
				break;
			default:
				for (String filePath: filePaths)
					if (validatePathExists(rootDir + "/" + filePath))
						validPathsSet.add(filePath);
		}

		// NOTE: Debug code.
		if (extension.equals(App.MODEL_EXTENSION)) {
			if (validPathsSet.isEmpty())
				throw new IllegalArgumentException("No valid model found for: " +
				                                   Arrays.toString(filePaths.toArray()));
			Utils.logDebug("Models to load: " + Arrays.toString(
							   validPathsSet.toArray(new String[0])));
		} else {
			if (validPathsSet.isEmpty())
				throw new IllegalArgumentException("No valid data source found for: " +
				                                   Arrays.toString(filePaths.toArray()));
			Utils.logDebug("Data source to work on: " + Arrays.toString(
							   validPathsSet.toArray(new String[0])));
		}

		return new ArrayList < String > (validPathsSet);
	}

	public static void
	validateDataSetSplits(Integer[] dataSetSplits) throws IllegalArgumentException {
		if (dataSetSplits.length < 2)
			throw new IllegalArgumentException(
				"The train & test data set split percentages are required: " +
				Arrays.toString(dataSetSplits));

		int sum = 0;
		int splitValue = dataSetSplits[0];

		if ((splitValue < 1) || (splitValue > 100))
			throw new IllegalArgumentException(
				"Invalid training data set split percentage (out of range 1-100): " +
				Arrays.toString(dataSetSplits));

		sum += splitValue;

		for (int iter = 1; iter < dataSetSplits.length; ++iter) {
			splitValue = dataSetSplits[iter];

			if ((splitValue < 0) || (splitValue > 100))
				throw new IllegalArgumentException(
					"Invalid data set split percentage (out of range 0-100): " +
					Arrays.toString(dataSetSplits));
			sum += splitValue;
		}

		if (sum < 100)
			Utils.logWarn(
				"The specified split percentages do not utilize the whole data set: " +
				Arrays.toString(dataSetSplits));
	}
}
