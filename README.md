# wekaMiner

A Java 11+ data mining tool using [Weka's](https://www.cs.waikato.ac.nz/~ml/weka/) libraries.

Uses
[![picocli](https://img.shields.io/badge/picocli-4.6.1-green.svg)](https://github.com/remkop/picocli)
for CLI parsing.

# Build release

To build an uber jar, execute:

```sh
gradle release
```

To build a distribution bundle, execute:

```sh
gradle distZip
```

or

```sh
gradle distTar
```

for a `zip` & `tar` respectively.

# Execution

## Using a gradle wrapper

Help

```sh
./gradlew run
```

Command execution format

```sh
./gradlew run --args"<arguments>"
```

E.g.:

```sh
./gradlew run --args="mine -r /home/user/test test-data.arff -c default"
```

This application accepts arff files, to convert csv files to arff run:

```sh
./gradlew run --args="convert -r <directory-with-csv> -I <csv-filename> \
-O <output-filename>"
```

## Using the created uber jar

**Should be located at "build/libs/WekaMiner-<version>-uber.jar"**

To use the jar execute:

```sh
java -jar <path-to-uber-jar> <subcommand> <arguments>
```

E.g.:

```
java -jar WekaMiner-1.3.0-uber.jar mine -r /home/user/test test-data.arff -c \
default -cl default
```

## Using the installDist output

**"build/install/WekaMiner" should be moved/symlinked to the most convenient
path for your usage.**

Using `installDist` save one the extraction step involved with using `distZip & distTar`. Execute `./gradlew installDis`, execute the project as
`./build/install/WekaMiner/bin/WekaMiner`. Or add
"build/install/WekaMiner/bin" to the `PATH`
environment variable.

## Using clusterer/classifier/filter options

Pass the quoted (") & spaced options for the desired item delmited by a colon (:).

**The option should have a space between the flag & value within the quotes,
but no space outside the quotes.**

Support for generated number sequences is available for "nearest neighbour" &
"number of clusters" situations.
This follows the classifier/clusterer's option for specifying either the
nearest neighbours/number of clusters.

E.g.:

```sh
./build/install/WekaMiner/bin/WekaMiner mine -r=/home/user/test all -s 100/0 \
-c=LibSVM,J48,IBk:-K3-30 -cl=EM:"-N 2-15":"-max 100",SimpleKMeans:"-N 2-15"

```

Item options can be obtained by executing:

```sh
./build/install/WekaMiner/bin/WekaMiner info \
<classifier/clusterer/filter/evaluator/searcher>
```

# Misc

1. There exists the file "libs/wekaServer.jar" that runs the data mining
   operations in parallel, an attempt to use it wasn't successful.

2. Previously hardcoded & based on course work on the matter:

   - Cluster sizes: 2-15 (odd numbers only)
   - Num neighbours: 3-30

The selection of values from a specified range will use all integers of the
range.

# Issues

1. Weka throws this exception:
   `java.util.zip.ZipException: zip END header not found`, something about
   `weka.core.ClassCache.initFromJar` fails, but the executable works.
2. The generation of reflections metadata will throw errors about
   "com.github.fommil.netlib"'s `UrlType` not being found; ignore this & its
   resultant errors.
