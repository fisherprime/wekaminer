import org.reflections.Reflections
import org.reflections.serializers.JsonSerializer
import org.reflections.util.ConfigurationBuilder
import java.net.URLClassLoader
import java.text.SimpleDateFormat
import java.util.Date

val projectName: String by project
val projectVersion: String by project

val javaVersion: String by project
val memoryLimit: String by project

val sourceForgeURL: String by project

val dom4jVersion: String by project
val gsonVersion: String by project
val guavaVersion: String by project
val javaxServletVersion: String by project
val junitVersion: String by project
val libSVMVersion: String by project
val picocliVersion: String by project
val reflectionsVersion: String by project
val slf4jApiVersion: String by project
val wekaDevVersion: String by project
val wekaServerVersion: String by project
val wekaStableVersion: String by project

// The `buildscript` block must appear before `plugins` block.
buildscript {
    val gsonVersion: String by project
    val reflectionsVersion: String by project
    val dom4jVersion: String by project
    val javaxServletVersion: String by project

    repositories {
        mavenCentral()
    }

    dependencies {
        classpath("com.google.code.gson:gson:$gsonVersion") {
            because("Needed by org.reflections.reflections for the `JsonSerializer`")
        }
        classpath("org.reflections:reflections:$reflectionsVersion")
        classpath("org.dom4j:dom4j:$dom4jVersion")
        classpath("javax.servlet:javax.servlet-api:$javaxServletVersion")
        // classpath ("com.github.fommil.netlib:all:${netlibVersion}")
    }
}

plugins {
    // Apply the java plugin to add support for Java.
    `java`

    // Apply the application plugin to add support for building an application.
    `application`
}

version = projectVersion

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(javaVersion))
    }
}

application {
    mainClass.set(projectName + ".App")

    // NOTE: These arguments achieve the following results:
    // Increase the maximum heap size;
    // Generate a heap dump on memory errors;
    // Set file encoding to `UTF-8`; &
    // Enable the use of the Z garbage Collector.
    applicationDefaultJvmArgs = listOf(
        "-Xmx$memoryLimit", "-XX:+HeapDumpOnOutOfMemoryError", "-Dfile.encoding=UTF-8",
        "-XX:+UnlockExperimentalVMOptions", "-XX:+UseZGC"
    )
}

val buildDateTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Date())
val sharedManifest = the<JavaPluginConvention>().manifest {
    attributes(
        "Buildtime" to buildDateTime,
        "Implementation-Title" to projectName,
        "Implementation-Vendor" to "Fisherprime",
        "Implementation-Version" to project.version,
        "Main-Class" to projectName + ".App"
    )
}

repositories {
    ivy {
        url = uri(sourceForgeURL)
        patternLayout {
            artifact("[organization]/[module][revision].[ext]")
        }
        metadataSources {
            artifact()
        }
        content {
            // This repository *only* contains artifacts with group "weka.weka-packages"
            includeGroup("weka/weka-packages")
        }
    }

    // Use mavenCentral to resolve dependencies.
    mavenCentral()

    // Unused, scan for dependencies on a locally-accessible filesystem.
    flatDir {
        dirs("$rootProject.projectDir/libs")
    }
}

dependencies {
    // Suppress kotlin language server warning.
    implementation(kotlin("script-runtime", "1.+"))

    // implementation ("org.apache.commons:commons-text:${commonsTextVersion}")

    implementation("info.picocli:picocli:$picocliVersion")

    implementation("org.reflections:reflections:$reflectionsVersion")
    implementation("com.google.code.gson:gson:$gsonVersion") {
        because("Needed by org.reflections.reflections for `JsonSerializer`")
    }
    implementation("org.slf4j:slf4j-api:$slf4jApiVersion") {
        because("Needed by org.reflections.reflections")
    }
    implementation("javax.servlet:javax.servlet-api:$javaxServletVersion") {
        because("Needed by org.reflections.reflections")
    }

    // implementation ("nz.ac.waikato.cms.weka:weka-stable:${wekaStableVersion}")
    implementation("nz.ac.waikato.cms.weka:weka-dev:$wekaDevVersion") {
        because("LibSVM depends on the dev version of weka")
    }
    implementation("nz.ac.waikato.cms.weka:LibSVM:$libSVMVersion")
    // implementation("weka/weka-packages:wekaServer:$wekaServerVersion@zip") {
    // because("Provides the parallel Weka task runner")
    // }

    // Use JUnit test framework
    testImplementation("junit:junit:$junitVersion")

    // Unused, allows for usage of locally available jars.
    implementation(fileTree(mapOf("dir" to "libs", "include" to listOf("*.jar"))))
}

tasks {
    compileJava {
        options.encoding = "UTF-8"
        options.isIncremental = true
    }

    register("updateReflections") {
        description = "Update the WekaMiner reflections manifest."

        dependsOn("compileJava")
        doLast {
            val reflectionsFile = "$buildDir/classes/java/main/META-INF/reflections/$projectName-reflections.json"

            // Capture URLs from the weka dependencies.
            val urls = project.configurations.runtimeClasspath.files
                .filter({ it.toString().indexOf("weka") > 0 })
                .map { it.toURL() }

            val config = ConfigurationBuilder()
                .addClassLoader(URLClassLoader(urls.toTypedArray()))
                .setUrls(urls)
                .useParallelExecutor()

            Reflections(config).save(reflectionsFile, JsonSerializer())
        }
    }

    jar {
        manifest = project.the<JavaPluginConvention>().manifest {
            from(sharedManifest)
        }

        dependsOn("updateReflections")
        from("LICENSE.md")
    }

    register<Jar>("UberJar") {
        archiveClassifier.set("uber")
        description = "Package the project & its dependencies into a executable jar."

        // The excluded library is unused.
        // excludes = listOf ("**/wekaServer.jar")

        from(sourceSets.main.get().output)

        dependsOn("configurations.runtimeClasspath")
        from({
            configurations.runtimeClasspath.filter {
                it.name.endsWith("jar")
            }.map { zipTree(it) }
        })
    }

    register("install") {
        description = "Run the `installDist` task after updating the reflections manifest."

        dependsOn("updateReflections")
        dependsOn("installDist")
    }
}
